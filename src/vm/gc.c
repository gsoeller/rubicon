#include <string.h>

#include "debug.h"
#include "gc.h"


HeapValue *evacuate(VM *vm, HeapValue *hv);
void scavenge(VM *vm);
void update_stack(VM *vm);

void
garbageCollect(VM *vm)
{
  debugGC("Running the garbage collector");

  /*
    GC Algorithm:
    Cheneys algorithm---copy live data from the heap to the to_space

    Roots:
    (1) Current activation frame
    (2) Top of the stack
    
    Algorithm
    (1) From the roots, copy live data depth-first.
    (2) On copy, update the info on the old data with a forwarding pointer.
    (3) Follow next pointer and jump to step 1 if the pointer points to the
        old heap.
    (4) Once all copying is completed, traverse the entire to_space, updating
        pointers that point into the old heap.
   */

  /*
    1. Initialize GC values
   */
  // The point we have scanned up to
  vm->scan_ptr = vm->to_space;
  // The address to copy to next
  vm->copy_ptr = vm->tp;

  /*
    2. Evacuate the roots
   */
  ActivationFrame *root =
    (ActivationFrame *) evacuate(vm, (HeapValue *) vm->af);
  vm->af->gcword = root;

  ESObject *globalObject =
    (ESObject *) evacuate(vm, (HeapValue *) vm->globalObject);
  vm->globalObject = globalObject;
  
  slot *top = vm->stack;
  while (top != NULL) {
    HeapValue *val = evacuate(vm, top->item);
    top->item->gcword = val;
    top->item = val;
    top = top->previous;
  }

  
  /*
    3. Scavenge the to_space
       The scavenger iterates through all the heap values in to_space,
       evacuating pointers into the from_heap.
   */
  scavenge(vm);

  /*
    4. Update pointers on the stack machine
  */
  update_stack(vm);
  
  /*
    5. Swap heaps
   */
  void *tmp_heap = vm->heap;
  vm->heap = vm->to_space;
  vm->hp = vm->copy_ptr;
  vm->to_space = tmp_heap;
  vm->tp = vm->to_space;
  
  memset(vm->to_space, 0, 4096);
  
  vm->af = root;

  vm->ready_to_gc = false;
  
  debugGC("Garbage collection complete!");
}

HeapValue *
copy(VM *vm, void *start, int size)
{
  if ((vm->copy_ptr + size) > (vm->to_space + 4096)) {
    debugGC("Not enough space to copy object!");
    exit(1);
  }
  HeapValue *to = (HeapValue *) vm->copy_ptr;
  memcpy(vm->copy_ptr, start, size);
  vm->copy_ptr += size;
  return to;
}

HeapValue *
evacuate(VM *vm, HeapValue *val)
{
  HeapValue *to = NULL;

  if (val == NULL) {
    debugGC("Evacuating NULL!");
    return val;
  }
  
  if (val->gcword != 0) {
    debugGC("Evacuating an object that has already been evacuated...");
    return (HeapValue *) val->gcword;
  }

  int size = sizeofHeapValue(val);

  // Move the object to the to_space
  to = copy(vm, (void *) val, size);
  to->gcword = NULL;
  // Make a forwarding pointer
  //val->gcword = to;

  return to;
}

void
scavenge(VM *vm)
{
  HeapValue *val;
  
  while (vm->scan_ptr < vm->copy_ptr) {
    val = (HeapValue *) vm->scan_ptr;
    if (0/*val->gcword == NULL*/) {
      debugGC("Scavenging an object that has already been evacuated. Moving on...");
    } else {
      debugGC("Scavenging (%d)", val->info);
      switch (val->info) {
      case NUMBER_INFO: {
        //evacuate(val);
        break;
      }
      case ID_INFO: {
        debugGC("Evacing ID: %s", ((HeapID *) val)->id);
        //evacuate(val);
        break;
      }
      case BOOL_INFO: {
        //evacuate(val);
        break;
      }
      case CODE_INFO: {
        HeapCode *code = (HeapCode *) val;
        //evacuate(val);
        code->name = (HeapID *) evacuate(vm, (HeapValue *) code->name);
        break;
      }
      case HEAP_NAMED_VALUE_INFO: {
        HeapNamedValue *nv = (HeapNamedValue *) val;
        //evacuate(val);
        nv->id = (HeapID *) evacuate(vm, (HeapValue *) nv->id);
        nv->val = evacuate(vm, nv->val);
        break;
      }
      case ACTIVATION_FRAME_INFO: {
        ActivationFrame *af = (ActivationFrame *) val;
        debugGC("Evacuating static link @ %p", af->staticLink);
        af->staticLink = (ActivationFrame *) evacuate(vm, (HeapValue *) af->staticLink);
        
        for (int i = 0; i < af->af_pointer; i++) {
          af->frame[i] = (HeapNamedValue *) evacuate(vm, (HeapValue *) af->frame[i]);
        }
        
        break;
      }
      case INFINITY_INFO: {
        break;
      }
      case NAN_INFO: {
        break;
      }
      case UNDEFINED_INFO: {
        //evacuate(val);
        break;
      }
      case SLOT_SAVE_INFO: {
        //evacuate(val);
        break;
      }
      case IP_INFO: {
        break;
      }
      case DATA_PROPERTY_INFO: {
        DataProperty *prop = (DataProperty *) val;
        prop->value = evacuate(vm, prop->value);
        break;
      }
      case ACCESSOR_PROPERTY_INFO: {
        AccessorProperty *acc = (AccessorProperty *) val;
        acc->get = evacuate(vm, acc->get);
        acc->set = evacuate(vm, acc->set);
        break;
      }
      case PROPERTY_INFO: {
        break;
      }
      case OBJECT_PROPERTY_INFO: {
        ObjectProperty *prop = (ObjectProperty *) val;
        prop->key = (HeapID *) evacuate(vm, (HeapValue *) prop->key);
        prop->value = (Property *) evacuate(vm, (HeapValue *) prop->value);
        break;
      }
      case OBJECT_INFO: {
        ESObject *obj = (ESObject *) val;
        for (int i = 0; i < obj->propertyCount; i++) {
          obj->properties[i] =
            (ObjectProperty *) evacuate(vm, (HeapValue *) obj->properties[i]);
        }
        break;
      }
      case CONSTRUCTOR_PROPERTY_INFO: {
        ConstructorProperty *prop = (ConstructorProperty *) val;
        prop->value = (ESObject *) evacuate(vm, (HeapValue *) prop->value);
        break;
      }
      case NATIVE_CODE_INFO: {
        NativeCode *nc = (NativeCode *) val;
        nc->name = (HeapID *) evacuate(vm, (HeapValue *) nc->name);
        break;
      }
      default: {
        debugGC("Garbage collector does not know how to scavenge object with info %d", val->info);
        exit(1);
      }
      }
    }
    int size = sizeofHeapValue(val);
    vm->scan_ptr += size;
    debugGC("Size of scavenged object = %d @ %p", size, val);
  }
}

void
update_stack(VM *vm)
{
  slot *top = vm->stack;

  while(top != NULL) {
    if (top->item->gcword != NULL) {
      debugGC("Updating pointer on stack from %p to %p", top->item, top->item->gcword);
      top->item = top->item->gcword;
    } else {
      debugGC("Item on stack did not move @ %p", top->item);
    }
    top = top->previous;
  }
}
