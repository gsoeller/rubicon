#define OPCODE_SIZE sizeof(int)

// Values
#define NUMBER_VAL           0
#define STRING_VAL           1
#define BOOL_VAL             2
#define CODE_VAL             3
#define ACTIVATION_FRAME_VAL 4
#define UNDEFINED_VAL        5
#define NULL_VAL             6
#define PINFINITY_VAL        7
#define NINFINITY_VAL        8
#define NAN_VAL              9

// Opcodes
#define RET                           100
#define PLUS                          101
#define MULTIPLY                      102
#define MINUS                         103
#define DIVIDE                        104
#define GT                            105
#define LT                            106
#define LTE                           107
#define GTE                           108
#define PUSH                          109
#define ACTIVATION_ALLOC_LOCAL        110
#define ACTIVATION_PUSH               111
#define ACTIVATION_PUSH_ARG           112
#define ACTIVATION_NAME_ARG           113
#define ACTIVATION_STORE              114
#define ACTIVATION_LOAD               115
#define ACTIVATION_CREATE             116
#define ACTIVATION_SWITCH             117
#define ACTIVATION_SWITCH_CONSTRUCTOR 118
#define ACTIVATION_REVERT             119
#define BIND                          120
#define LABEL                         121
#define JMP_TRUE                      122
#define WHILE_COND                    123
#define JMP_WHILE_TRUE                124
#define END_BODY                      125
#define JMP_TOP                       126
#define LOAD_MEMBER                   127

#define CODE_END 1000


#define RETURN_UNDEFINED 0
#define RETURN_NULL      1
#define RETURN_OBJECT    2

#define RETURN_BOOLEAN 0
#define RETURN_STRING  1
#define RETURN_SYMBOL  2
#define RETURN_NUMBER  3

#define RETURN_REAL      0
#define RETURN_PINFINITY 0
#define RETURN_NINFINITY 1
#define RETURN_NAN       2


#define UNDEFINED_TMP 999999
