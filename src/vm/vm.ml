
exception ReferenceError of string
exception ActivationFrameOOM
        
type vm
   
type activation_frame_size = int
[@@deriving show]
                               
type code = bc list * activation_frame_size
and args = string list
                   
and fundef =
    Fundef of code * args
       
and frame_env = (string, int) Hashtbl.t * (string, fundef) Hashtbl.t
[@printer fun _ _ -> ()]
[@@deriving show]

and frame =
  | EmptyFrame
  | Frame of stack_value list * frame ref * bc list * frame_env
[@@deriving show]

and stack_value =
  | JSValue of js_type
  | Code of bc list
  | ActivationFrame of frame
  | ClosurePointer of int (* Raw pointer to value *)
[@@deriving show]

and number =
  | Real of float
  | PInfinity
  | NInifinity
  | NaN
[@@deriving show]

and js_type =
  | Undefined
  | Null
  | Boolean of bool
  | String of string
  | Symbol of string
  | Number of number
  | Object
[@@deriving show, equal]
      
and bc =
  | Ret
  | Plus
  | Multiply
  | Minus
  | Divide
  | GreaterThan
  | LessThan
  | LessThanEqualTo
  | GreaterThanEqualTo
  | Push of stack_value
  | ActivationAllocLocal
  | ActivationPush
  | ActivationPushArg
  | ActivationNameArg
  | ActivationStore
  | ActivationLoad
  | ActivationCreate
  | ActivationSwitch
  | ActivationSwitchConstructor
  | ActivationRevert
  | Bind 
  | Label of string * bc list
  | JmpTrue
  | WhileCond
  | JmpWhileTrue
  | EndBody
  | JmpTop
  | LoadMember
[@@deriving show]

external emit_bc      : vm ->bc -> unit = "emit_bc"
external interpretVM  : vm -> js_type = "interpretVM"
external cleanupVM    : vm -> unit    = "cleanupVM"
external initializeVM : unit -> vm    = "initializeVM"

let initializeVM () =
  Error.initialize_exceptions ();
  initializeVM ()
