#ifndef SAHARA_H
#define SAHARA_H

#include "heap.h"

HeapValue *interpret(VM *vm);

#endif
