#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "opcodes.h"
#include "stack.h"
#include "debug.h"
#include "vm.h"
#include "numbers.h"
#include "gc.h"

int
size_of_heap_value(HeapValue *val)
{
  switch (val->info) {
  case NUMBER_INFO: return sizeof(HeapNumber);
  case ID_INFO: return sizeof(HeapID);
  case BOOL_INFO: return sizeof(HeapBool);
  case CODE_INFO: return sizeof(HeapCode); //TODO will fail
  case ACTIVATION_FRAME_INFO:
    return sizeof(ACTIVATION_FRAME_INFO); // TODO will fail
  case UNDEFINED_INFO: return sizeof(UNDEFINED_INFO);
  }
}

void
activationRevert(VM *vm)
{
  HeapValue *ret = pop(vm);
        
  while (ret->info == ID_INFO) {
    switch (ret->info) {
    case ID_INFO: {
      HeapID *name = (HeapID *) ret;
      HeapNamedValue *nv = find_var(name->id, vm->af);
      ret = nv->val;
      break;
    }
    }
  }
  push(vm, ret);
  vm->af = vm->af->staticLink;
}

HeapValue *
interpret_val(VM *vm)
{
  int opcode = *(int *)vm->af->ip;
  //("Val opcode: %d %f %p\n", opcode, (double) opcode, af->ip);
  vm->af->ip += sizeof(int);
  switch (opcode) {
  case NUMBER_VAL: {
    double val = *(double *)vm->af->ip;
    vm->af->ip += sizeof(double);
    HeapNumber *lit = (HeapNumber *) sahara_alloc(vm, sizeof(HeapNumber));
    lit->info = NUMBER_INFO;
    lit->value = val;
    return (HeapValue *)lit;
  }
  case STRING_VAL: {
    int len = strlen((char *)vm->af->ip);
    HeapID *id = (HeapID *) sahara_alloc(vm, sizeof(HeapID) + len);
    id->info = ID_INFO;
    id->size = len;
    for (int i = 0; i < len - 1; i++) {
      id->id[i] = *(char *)vm->af->ip;
      vm->af->ip++;
    }
    id->id[len] = '\0';
    return (HeapValue *)id;
  }
  case BOOL_VAL: {
    HeapBool *hBool = (HeapBool *) sahara_alloc(vm, sizeof(HeapBool));
    int b = *(int *)vm->af->ip; 
    hBool->info = BOOL_INFO;
    hBool->hBool = b;
    vm->af->ip += sizeof(int);
    return (HeapValue *) hBool;
  }
  case CODE_VAL: {
    //printf("Code val %p\n", af->ip);
    int *endOfCode = *(int **) vm->af->ip;
    vm->af->ip += sizeof(int);
    HeapCode *code = (HeapCode *) sahara_alloc(vm, sizeof(HeapCode));
    code->info = CODE_INFO;
    code->code = vm->af->ip;
    vm->af->ip = endOfCode;

    return (HeapValue *) code;
  }
  case UNDEFINED_VAL: {
    //printf("Undefined val\n");

    Undefined *undefined = (Undefined *) sahara_alloc(vm, sizeof(Undefined));
    undefined->info = UNDEFINED_INFO;
    
    return (HeapValue *) undefined;
  }
  }
  return NULL;
}

void
interpret_next(VM *vm)
{
  int opcode = *(int *)vm->af->ip;
  debugBC("Opcode: %d %f %p", opcode, (double) opcode, vm->af->ip);
  vm->af->ip += sizeof(int);
  switch (opcode) {
    /*case CODE_VAL: {
    int code = 0;
    while (code != CODE_END) {
      code = *(int *)ip;
      interpret_next();
    }
    printf("Code end..\n");
    ip += sizeof(int);
    break;
  }
  case ACTIVATION_FRAME_VAL: printf("activation error"); break;*/
  case RET: {
    debugBC("RET");
    HeapValue *ret = vm->ret_register;
    push(vm, ret);
    break;
  }
  case PLUS: {
    interpret_binop(vm, '+');
    break; 
  }
  case MULTIPLY: {
    interpret_binop(vm, '*');
    break; 
  }
  case MINUS: {
    interpret_binop(vm, '-');
    break; 
  }
  case DIVIDE: {
    interpret_binop(vm, '/');
    break; 
  }
  case GT: {
    debugBC("GT");
    interpret_boolop(vm, '>');
    break;
  }
  case LT: {
    debugBC("LT");
    interpret_boolop(vm, '<');
    break;
  }
  case LTE: {
    debugBC("LTE");
    interpret_boolop(vm, 64);
    break;
  }
  case GTE: {
    debugBC("GTE");
    interpret_boolop(vm, 65);
    break;
  }
  case ACTIVATION_ALLOC_LOCAL: {
    //void *index = sp;
    //sp += sizeof(int);
    //push(index);
    break;
  }
  case ACTIVATION_PUSH: {

    HeapValue *hv   = pop(vm);    
    HeapValue *data = pop(vm);
    
    HeapNamedValue *nv = (HeapNamedValue *)
      sahara_alloc(vm, sizeof(HeapNamedValue));
    nv->info = HEAP_NAMED_VALUE_INFO;
    nv->id = (HeapID *) hv;
    nv->val = data;

    memcpy(&(vm->af->frame[vm->af->af_pointer]), &nv, sizeof(HeapNamedValue *));
    vm->af->af_pointer++;
    vm->af->name_pointer++;

    save_ret(vm, (HeapValue *) nv);
    
    break;
  }
  case ACTIVATION_PUSH_ARG: {

    debugBC("Activation push arg");
    
    HeapValue *val = pop(vm);

    ActivationFrame *calleeAF = (ActivationFrame *) vm->intermediate_register;

    HeapNamedValue *nv = (HeapNamedValue *)
      sahara_alloc(vm, sizeof(HeapNamedValue));
    nv->info = HEAP_NAMED_VALUE_INFO;
    nv->id = NULL;

    // Need to push a pointer to the value, not the name
    switch(val->info) {
    case ID_INFO: {
      HeapID *id = (HeapID *) val;
      HeapNamedValue *hv = find_var(id->id, vm->af);
      nv->val = hv->val;
      break;
    } default: {
        nv->val = val;
      }
    }

    memcpy(&(calleeAF->frame[calleeAF->af_pointer]),
           &nv,
           sizeof(HeapNamedValue *));
    calleeAF->af_pointer++;
    
    break;
  }
  case ACTIVATION_NAME_ARG: {
    debugBC("Naming activation arg");

    HeapID *name = (HeapID *) pop(vm);

    if (vm->af->frame[vm->af->name_pointer] == NULL) {
      // The argument was not passed by the caller
       HeapNamedValue *nv = (HeapNamedValue *)
         sahara_alloc(vm, sizeof(HeapNamedValue));
       Undefined *undf = (Undefined *) sahara_alloc(vm, sizeof(Undefined));
       undf->info = UNDEFINED_INFO;
       nv->info = HEAP_NAMED_VALUE_INFO;
       nv->id = name;
       nv->val = (HeapValue *) undf;
       memcpy(&(vm->af->frame[vm->af->name_pointer]),
           &nv,
           sizeof(HeapNamedValue *));
    } else {
      vm->af->frame[vm->af->name_pointer]->id = name;
    }
    vm->af->name_pointer++;
    break;
  }
  case ACTIVATION_STORE: {

    HeapID *id     = (HeapID *) pop(vm);
    HeapValue *val =            pop(vm);

    HeapNamedValue *slot = find_var(id->id, vm->af);
    slot->val = val;

    push(vm, (HeapValue *) id);
    
    break;
  }
  case ACTIVATION_LOAD: {

    HeapID *id = (HeapID *) pop(vm);
    HeapNamedValue *slot = find_var(id->id, vm->af);
    push(vm, (HeapValue *)slot->id);
    
    break;
  }
  case ACTIVATION_CREATE: {
    HeapValue *fun = pop(vm);

    int numLocals;
    int codeType;
    void *codePointer;

    LOAD_FUNCTION:
    switch (fun->info) {
    case NATIVE_CODE_INFO: {
      codeType = NATIVE_CODE_INFO;
      codePointer = (void *) ((NativeCode *) fun)->code;
      if (((NativeCode *) fun)->num_locals == 0) {
        numLocals = 10;
      } else {
        numLocals = ((NativeCode *) fun)->num_locals;
      }
      break;
    }
    case CODE_INFO: {
      codeType = CODE_INFO;
      codePointer = (void *) ((HeapCode *) fun)->code;
      if (((HeapCode *) fun)->num_locals == 0) {
        numLocals = 10;
      } else {
        numLocals = ((HeapCode *) fun)->num_locals;
      }
      break;
    }
    case ID_INFO: {
      char *id = ((HeapID *) fun)->id;
      fun = find_var(id, vm->af)->val;
      goto LOAD_FUNCTION;
    }
    case OBJECT_INFO: {
      ESObject *obj = (ESObject *) fun;
      HeapNumber *length = (HeapNumber *) lookupMember(vm, obj, "length");
      numLocals = length->value;
      codeType = OBJECT_CODE_INFO;
      codePointer = (void *) obj;
      break;
    }
    default: {
      debugBC("Cannot create a frame from info %d", fun->info);
      exit(1);
    }
    }
    
    ActivationFrame *frame =
      allocActivationFrame(vm, numLocals);
    frame->ip = codePointer;
    frame->codeType = codeType;
    push(vm, (HeapValue *) frame);

    vm->intermediate_register = (HeapValue *) frame;
    
    break;
  }
  case ACTIVATION_SWITCH: {
    
    ActivationFrame *callee = (ActivationFrame *) pop(vm);
    callee->staticLink = vm->af;
    vm->af = callee;
    
    switch (vm->af->codeType) {
    case NATIVE_CODE_INFO: {
      nativeFun callee_ = (nativeFun) vm->af->ip;
      callee_(vm);
      activationRevert(vm);
      break;
    }
    case OBJECT_CODE_INFO: {
      debugBC("Executing [[call]] property of object");
      ESObject *obj = (ESObject *) vm->af->ip;
      exit(1);
      break;
    }
    }
    break;
  }
    // WARNING: DO NOT INSERT CASE HERE
    // FALL THROUGH IS REQUIRED FOR ACTIVATION_CREATE
  case ACTIVATION_REVERT: {

    activationRevert(vm);
    
    break;
  }
  case ACTIVATION_SWITCH_CONSTRUCTOR: {
    debugBC("Executing [[construct]] property of object");

    ActivationFrame *callee = (ActivationFrame *) pop(vm);
    callee->staticLink = vm->af;
    vm->af = callee;
    
    ESObject *obj = (ESObject *) vm->af->ip;
    NativeCode *constructor =
      (NativeCode *) lookupMember(vm, obj, "constructor");
    constructor->code(vm);
    activationRevert(vm);
 
    break;
  }
  case BIND: {
    
    HeapCode *code = (HeapCode *) pop(vm);
    HeapID   *name = (HeapID *)   pop(vm);
 
    code->num_locals = 10;
    code->name      = name;

    HeapNamedValue *nv = (HeapNamedValue *) 
      sahara_alloc(vm, sizeof(HeapNamedValue));
    nv->info = HEAP_NAMED_VALUE_INFO;
    nv->id   = name;
    nv->val  = (HeapValue *) code;
   
    memcpy(&(vm->af->frame[vm->af->af_pointer]), &nv, sizeof(HeapNamedValue *));
    vm->af->af_pointer++;
    vm->af->name_pointer++;

    break;
  }
  case JMP_TOP: {
    debugBC("Jmp top");
    exit(1);
    break;
  }
  case JMP_TRUE: {
    debugBC("jmp true");

    HeapBool *hBool  = (HeapBool *) pop(vm);
    HeapCode *else_ = (HeapCode *) pop(vm);
    HeapCode *if_   = (HeapCode *) pop(vm);

    IP *ip_save = (IP *) sahara_alloc(vm, sizeof(IP));
    ip_save->info = IP_INFO;
    ip_save->ip = vm->af->ip;
    
    push(vm, (HeapValue *) ip_save);
    
    if (hBool->hBool == 0) {
      vm->af->ip = else_->code;
    } else {
      vm->af->ip = if_->code;
    }
    break;
  }
  case END_BODY: {
    debugBC("End body");
    pop_context(vm);
    HeapCode *cond = (HeapCode *) pop(vm);
    push(vm, (HeapValue *)cond);
    vm->af->ip = cond->code;
    break;
  }
  case WHILE_COND: {
    debugBC("while cond");
    HeapCode *cond = (HeapCode *) pop(vm);
    HeapCode *body = (HeapCode *) pop(vm);

    IP *ip_save = (IP *) sahara_alloc(vm, sizeof(IP));
    ip_save->info = IP_INFO;
    ip_save->ip = vm->af->ip - sizeof(int);
    
    push(vm, (HeapValue *) ip_save);
    push(vm, (HeapValue *) body);
    push(vm, (HeapValue *) cond);
    
    vm->af->ip = cond->code;

    break;
  }
  case JMP_WHILE_TRUE: {
    debugBC("jmp while true");
    HeapBool *cond_res = (HeapBool *) pop(vm);
    HeapCode *cond     = (HeapCode *) pop(vm);
    HeapCode *body     = (HeapCode *) pop(vm);

    if (cond_res->hBool == 0) {
      IP *ip_save = (IP *) pop(vm);
      vm->af->ip = ip_save->ip + sizeof(int);
    } else {
      push(vm, (HeapValue *) body);
      push(vm, (HeapValue *) cond);
      vm->af->ip = body->code;
      push_context(vm);
    }
    break;
  }
  case LOAD_MEMBER: {
    debugBC("load member");
    HeapValue *property = (HeapValue *) pop(vm);
    HeapValue *base     = (HeapValue *) pop(vm);

    switch (base->info) {
    case ID_INFO: {
      char *base_id = ((HeapID *) base)->id;
      debugBC("Loading base ID: %s", base_id);
      HeapNamedValue *base_ = find_var(base_id, vm->af);
      switch (base_->val->info) {
      case OBJECT_INFO: {
        HeapValue *member =
          lookupMember(vm, (ESObject *) base_->val, ((HeapID *) property)->id);
        push(vm, member);
        break;
      }
      default: {
        debugBC("Expected an object!");
        exit(1);
      }
      }
      break;
    }
    default: {
      debugBC("Cannot load a base of type %d", base->info);
      exit(1);
    }
    }
    break;
  }
  case PUSH: {
    HeapValue *val = interpret_val(vm);
    push(vm, val);
    break;
  }
  case LABEL: {
    int len = strlen((char *)vm->af->ip) - 1;
    for (int i = 0; i < len; i++) {
      vm->af->ip++;
    }
    interpret_next(vm);
  }
  default: {
    debugBC("Unknown opcode");
  }
  }
}

HeapValue *
interpret(VM *vm)
{
  debugBC("Interpreting code with Sahara....");

  vm->af           = allocActivationFrame(vm, 100);
  vm->af->ip       = vm->code_heap;
  vm->globalObject = makeGlobalObject(vm);
  
  while (*(int *)vm->af->ip != RET) {
    interpret_next(vm);
    
    if (vm->ready_to_gc) {
      debugGC("Allocator went over limit. Running GC before next opcode.");
      garbageCollect(vm);
    }
  }

  HeapValue *res = pop(vm);
  return res;
  
  
}
