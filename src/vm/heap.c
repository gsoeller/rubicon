#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/mman.h>

#include "debug.h"
#include "gc.h"
#include "exception.h"
#include "vm.h"

int size = 4096;

VM *
initializeHeap()
{
  debugBC("Initializing a new VM...");

  VM *vm = malloc(sizeof(VM));
  
  vm->heap      = mmap(NULL, size, PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
  vm->code_heap = mmap(NULL, size, PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
  vm->to_space  = mmap(NULL, size, PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);

  vm->hp      = vm->heap;
  vm->code_hp = vm->code_heap;
  vm->tp      = vm->to_space;
  
  vm->af           = NULL;
  vm->ret_register = NULL;
  vm->stack        = NULL;

  vm->scan_ptr    = NULL;
  vm->copy_ptr    = NULL;
  vm->ready_to_gc = false;

  return vm;
}

void
cleanup(VM *vm)
{
  munmap(vm->heap,      size);
  munmap(vm->to_space,  size);
  munmap(vm->code_heap, size);

  vm->heap      = NULL;
  vm->code_heap = NULL;
  vm->code_hp   = NULL;
  vm->to_space  = NULL;
  vm->hp        = NULL;
  vm->tp        = NULL;
  
  vm->af = NULL;

  vm->ret_register          = NULL;
  vm->intermediate_register = NULL;

  vm->stack = NULL;
  
  vm->scan_ptr    = NULL;
  vm->copy_ptr    = NULL;
  vm->ready_to_gc = false;
}

int
sizeofHeapValue(HeapValue *val)
{
  switch (val->info) {
  case NUMBER_INFO: {
    return sizeof(HeapNumber);
  }
  case ID_INFO: {
    return sizeof(HeapID) + ((HeapID *) val)->size;
  }
  case BOOL_INFO: {
    return sizeof(HeapBool);
  }
  case CODE_INFO: {
    return sizeof(HeapCode);
  }
  case HEAP_NAMED_VALUE_INFO: {
    return sizeof(HeapNamedValue);
  }
  case ACTIVATION_FRAME_INFO: {
    return sizeof(ActivationFrame) +
      (sizeof(HeapNamedValue *) * ((ActivationFrame *) val)->num_locals);
  }
  case INFINITY_INFO: {
    return sizeof(Infinity);
  }
  case NAN_INFO: {
    return sizeof(NaN);
  }
  case UNDEFINED_INFO: {
    return sizeof(Undefined);
  }
  case SLOT_SAVE_INFO: {
    return sizeof(SlotSave);
  }
  case IP_INFO: {
    return sizeof(IP);
  }
  case DATA_PROPERTY_INFO: {
    return sizeof(DataProperty);
  }
  case ACCESSOR_PROPERTY_INFO: {
    return sizeof(AccessorProperty);
  }
  case PROPERTY_INFO: {
    return sizeof(Property);
  }
  case OBJECT_PROPERTY_INFO: {
    return sizeof(ObjectProperty);
  }
  case OBJECT_INFO: {
    return sizeof(ESObject) +
      (sizeof(ObjectProperty *) * ((ESObject *) val)->propertyCount);
  }
  case CONSTRUCTOR_PROPERTY_INFO: {
    return sizeof(ConstructorProperty);
  }
  case NATIVE_CODE_INFO: {
    return sizeof(NativeCode);
  }
  default: {
    debugBC("Unknown size with info %d", val->info); exit(1);
  }
  }
}

/*
  Allocates space on the heap. This can be dangerous to use. It is
  only safe if there is not a HeapValue in scope that will be referenced.
  If there is one, use sahara_alloc_no_gc instead. That will tell the VM
  to GC at a safe point in the runtime execution.
 */
HeapValue *
sahara_alloc(VM *vm, int required)
{
  if ((vm->hp + required) > (vm->heap + size)) {
    debugBC("Heap is too big!");
    garbageCollect(vm);
  }
  HeapValue *hv = vm->hp;
  hv->gcword = 0;
  vm->hp += required;
  return hv;
}

HeapValue *
sahara_alloc_no_gc(VM *vm, int required)
{
  HeapValue *hv;
  if ((vm->hp + required) > (vm->heap + size)) {
    debugBC("Heap is too big! Allocating into the to space!");
    hv = vm->tp;
    vm->tp += required;
    vm->ready_to_gc = true;
  } else {
    hv = vm->hp;
    vm->hp += required;
  }
  hv->gcword = 0;
  return hv;
}

ActivationFrame *
allocActivationFrame(VM *vm, int numLocals)
{
  int namedValuesSize = sizeof(HeapNamedValue *) * numLocals;
  int allocSize = namedValuesSize + sizeof(ActivationFrame);
  ActivationFrame *af = (ActivationFrame *) sahara_alloc(vm, allocSize);
  af->num_locals   = numLocals;
  af->info         = ACTIVATION_FRAME_INFO;
  af->af_pointer   = 0;
  af->name_pointer = 0;
  for (int i = 0; i < namedValuesSize; i++) {
    //HeapNamedValue *nv = (HeapNamedValue *)
    //  sahara_alloc(sizeof(HeapNamedValue));
    //nv->id = NULL;
    //nv->val = NULL;
  }
  return af;
}

HeapNamedValue *
find_var(char *id, ActivationFrame *af_)
{
  if (af_ == NULL) {
    goto unknown_variable;
  } 
  
  HeapNamedValue *val = NULL;
  while (af_ != NULL && af_->info != UNDEFINED_INFO) {
    for (int i = 0; i < af_->af_pointer || i < af_->name_pointer; i++) {
      val = (HeapNamedValue *) (af_->frame[i]);
      if (val == NULL) continue;
      debugBC("Checking %s", val->id->id);
      if (strcmp(val->id->id, id) == 0) {
        debugBC("Found the variable...");
        return val;
      }
    }
    af_ = af_->staticLink;
  }
 unknown_variable:
  debugBC("Unknown variable %s", id);
  raise_reference_error(id);
}



HeapValue *
lookupMember(VM *vm, ESObject *base, char *property)
{
  for (int i = 0; i < base->propertyCount; i++) {
    ObjectProperty *prop = base->properties[i];
    if (strcmp(prop->key->id, property) == 0) {
      debugBC("Found the property `%s`", property);
      switch (prop->info) {
      case DATA_PROPERTY_INFO: {
        return ((DataProperty *) prop)->value;
      }
      case CONSTRUCTOR_PROPERTY_INFO: {
        return (HeapValue *) ((ConstructorProperty *) prop)->value;
      }
      case OBJECT_PROPERTY_INFO: {
        switch (prop->value->info) {
        case DATA_PROPERTY_INFO: {
          return ((DataProperty *) prop->value)->value;
        }
        case CONSTRUCTOR_PROPERTY_INFO: {
          return (HeapValue *) prop->value;
        }
        default: {
          debugBC("Unknown");
          exit(1);
        }
        }
      }
      case ACCESSOR_PROPERTY_INFO: {
        debugBC("Cannot load an accessor property");
        exit(1);
      }
      }
    }
  }
  debugBC("Could not find property");
  exit(1);
}
