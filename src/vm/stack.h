#ifndef STACK_H
#define STACK_H

#include "object.h"
#include "heap.h"

void save_ret(VM *vm, HeapValue *hv);

void push(VM *vm, HeapValue *item);
HeapValue *pop(VM *vm);

void pop_context(VM *vm);
void push_context(VM *vm);

#endif
