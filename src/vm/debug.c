#include "debug.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
/*void
debug(int system, char *msg, ...)
{
  va_list argp;
  
  switch (system) {
  case BC: {
    #ifdef DEBUG_BC
    printf("BC: %s\n", msg);
    #endif
  }
  }
}
*/

static const char *level_names[] = {
  "Bytecode", "GC"
};

static const char *level_colors[] = {
                                     "\x1b[94m", "\x1b[36m"/*, "\x1b[32m", "\x1b[33m", "\x1b[31m", "\x1b[35m"*/
};

void
debug(int system, const char *file, int line, const char *fmt, ...)
{
  /* Get current time */
  time_t t = time(NULL);
  struct tm *lt = localtime(&t);
  va_list args;
  char buf[16];
  buf[strftime(buf, sizeof(buf), "%H:%M:%S", lt)] = '\0';
  //#ifdef LOG_USE_COLOR
  fprintf(
          stderr, "%s %s%-5s\x1b[0m \x1b[90m%s:%d:\x1b[0m ",
          buf, level_colors[system], level_names[system], file, line);
  //#else
  //  fprintf(stderr, "%s %-5s %s:%d: ", buf, level_names[system], file, line);
  //#endif
  va_start(args, fmt);
  vfprintf(stderr, fmt, args);
  va_end(args);
  fprintf(stderr, "\n");
  fflush(stderr);
}
