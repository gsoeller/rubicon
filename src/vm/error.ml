exception ReferenceError of string
exception ActivationFrameOOM
        
let initialize_exceptions () =
  Callback.register_exception "reference error" (ReferenceError "");
  Callback.register_exception "activation oom" ActivationFrameOOM
