
#include <stdbool.h>
#include <stdlib.h>

#include "vm.h"
#include "object.h"
#include "stack.h"
#include "heap.h"
#include "debug.h"
#include "object_util.h"

// es: 7.1.3
HeapValue *
toNumber(VM *vm, HeapValue *res)
{ 
  HeapValue *hv = res;
  
  while(1) {
    switch (hv->info) {
    case NUMBER_INFO: {
      //double val = ((HeapNumber *) hv)->value;
      return hv;
    }
    case ID_INFO: {
      HeapID *id = (HeapID *) hv;
      char *id_ = id->id;
      HeapNamedValue *nv = find_var(id_, vm->af);
      hv = (HeapValue *)nv;
      break;
    }
    case BOOL_INFO: {
      HeapBool *hBool = (HeapBool *) hv;
      HeapNumber *cast =
        (HeapNumber *) sahara_alloc_no_gc(vm, sizeof(HeapNumber));
      cast->info = NUMBER_INFO;
      if (hBool->hBool == 0) {
        cast->value = 0;
      } else {
        cast->value = 1;
      }
      return (HeapValue *) cast;
    }
    case UNDEFINED_INFO: {
      NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
      nan->info = NAN_INFO;
      return (HeapValue *) nan;
    }
    case NULL_INFO: {
      HeapNumber *cast =
        (HeapNumber *) sahara_alloc_no_gc(vm, sizeof(HeapNumber));
      cast->info = NUMBER_INFO;
      cast->value = 0;
      return (HeapValue *) cast;
    }
    case HEAP_NAMED_VALUE_INFO: {
      hv = ((HeapNamedValue *) hv)->val;
      break;
    }
    case NAN_INFO: {
      return hv;
    }
    default: {
      debugBC("Load double type not implemented yet (%d)", hv->info);
      exit(1);
    }
    }
  }
}

bool
isNaN(HeapValue *hv)
{
  switch(hv->info) {
  case NAN_INFO: {
    return true;
  } default: {
      return false;
    }
  }
}

bool
isPInfinity(HeapValue *hv)
{
  switch(hv->info) {
  case INFINITY_INFO: {
    switch(((Infinity *)hv)->direction) {
    case NONE: {
      return false;
    }
    case POSITIVE: {
      return true;
    }
    case NEGATIVE: {
      return false;
    }
    }
  }
  default: {
    return false;
  }
  }
}

bool
isNInfinity(HeapValue *hv)
{
  switch(hv->info) {
  case INFINITY_INFO: {
    switch(((Infinity *)hv)->direction) {
    case NONE: {
      return false;
    }
    case POSITIVE: {
      return false;
    }
    case NEGATIVE: {
      return true;
    }
    }
  }
  default: {
    return false;
  }
  }
}

bool
isInfinity(HeapValue *hv)
{
  switch(hv->info) {
  case INFINITY_INFO: {
    return true;
  }
  default: {
    return false;
  }
  }
}

bool
isZero(HeapValue *hv)
{
  switch(hv->info) {
  case NUMBER_INFO: {
    if (((HeapNumber *) hv)->value == 0) {
      return true;
    }  
  } default: {
      return false;
    }
  }
}

bool
isLiteral(HeapValue *hv)
{
  switch(hv->info) {
  case NUMBER_INFO: {
    return true;
  } default: {
      return false;
    }
  }
}

// es: 12.8.5
HeapValue *
additive(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
  if (isNaN(lhs) || isNaN(rhs)) {
    NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
    nan->info = NAN_INFO;
    return (HeapValue *) nan;
  }

  if (isInfinity(lhs) && isInfinity(rhs)) {
    if (isPInfinity(lhs) && isNInfinity(rhs)) {
      NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
      nan->info = NAN_INFO;
      return (HeapValue *) nan;
    } else if (isPInfinity(rhs) && isNInfinity(lhs)) {
      NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
      nan->info = NAN_INFO;
      return (HeapValue *) nan;
    } else {
      Infinity *inf = (Infinity *) sahara_alloc_no_gc(vm, sizeof(Infinity));
      inf->info = INFINITY_INFO;
      inf->direction = ((Infinity *) lhs)->direction;
      return (HeapValue *) inf;
    }
  }

  if (isInfinity(lhs)) {
    Infinity *inf = (Infinity *) sahara_alloc_no_gc(vm, sizeof(Infinity));
    inf->info = INFINITY_INFO;
    inf->direction = ((Infinity *) lhs)->direction;
    return (HeapValue *) inf;
  }

  if (isInfinity(rhs)) {
    Infinity *inf = (Infinity *) sahara_alloc_no_gc(vm, sizeof(Infinity));
    inf->info = INFINITY_INFO;
    inf->direction = ((Infinity *) rhs)->direction;
    return (HeapValue *) inf;
  }

  // TODO: handle +0 and -0 cases

  return NULL;
}

HeapValue *
add(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
  
  HeapValue *res = additive(vm, lhs, rhs);
  if (res == NULL) {
    HeapNumber *lhs_ = (HeapNumber *) lhs;
    HeapNumber *rhs_ = (HeapNumber *) rhs;

    debugBC("\n\n%f + %f\n", lhs_->value, rhs_->value);
    
    HeapNumber *sum =
      (HeapNumber *) sahara_alloc_no_gc(vm, sizeof(HeapNumber));
    sum->info = NUMBER_INFO;
    sum->value = lhs_->value + rhs_->value;
    return (HeapValue *) sum;
  }
  return res;
}

HeapValue *
subtract(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
 
  HeapValue *res = additive(vm, lhs, rhs);
  if (res == NULL) {
    HeapNumber *lhs_ = (HeapNumber *) lhs;
    HeapNumber *rhs_ = (HeapNumber *) rhs;

    HeapNumber *sub =
      (HeapNumber *) sahara_alloc_no_gc(vm, sizeof(HeapNumber));
    sub->info = NUMBER_INFO;
    sub->value = lhs_->value - rhs_->value;
    return (HeapValue *) sub;
  }
  return res;
}

// es: 12.7.3.1
HeapValue *
multiply(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
  if (isNaN(lhs) || isNaN(rhs)) {
    NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
    nan->info = NAN_INFO;
    return (HeapValue *) nan;
  }

  if(!isInfinity(lhs) && isInfinity(rhs)) {
    if (isZero(lhs)) {
      NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
      nan->info = NAN_INFO;
      return (HeapValue *) nan;
    } else {
      Infinity *inf = (Infinity *) sahara_alloc_no_gc(vm, sizeof(Infinity));
      inf->info = INFINITY_INFO;
      inf->direction = ((Infinity *) rhs)->direction;
      return (HeapValue *) inf;
    }
  }

  if(!isInfinity(rhs) && isInfinity(lhs)) {
    if (isZero(rhs)) {
      NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
      nan->info = NAN_INFO;
      return (HeapValue *) nan;
    } else {
      Infinity *inf = (Infinity *) sahara_alloc_no_gc(vm, sizeof(Infinity));
      inf->info = INFINITY_INFO;
      inf->direction = ((Infinity *) lhs)->direction;
      return (HeapValue *) inf;
    }
  }

  if (isInfinity(lhs) && isInfinity(rhs)) {
    Infinity *inf = (Infinity *) sahara_alloc_no_gc(vm, sizeof(Infinity));
    inf->info = INFINITY_INFO;
    if (isPInfinity(lhs) && isPInfinity(rhs)) {
      inf->direction = POSITIVE;
    } else if (isPInfinity(lhs) && isNInfinity(rhs)) {
      inf->direction = NEGATIVE;
    } else if (isNInfinity(lhs) && isPInfinity(rhs)) {
      inf->direction = NEGATIVE;
    } else {
      inf->direction = POSITIVE;
    }
    return (HeapValue *) inf;
  }

  HeapNumber *res =
    (HeapNumber *) sahara_alloc_no_gc(vm, sizeof(HeapNumber));
  res->info = NUMBER_INFO;
  res->value = ((HeapNumber *) lhs)->value * ((HeapNumber *) rhs)->value;
  return (HeapValue *) res;
}

// es: 12.7.3.2
HeapValue *
divide(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
  if (isNaN(lhs) || isNaN(rhs)) {
    NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
    nan->info = NAN_INFO;
    return (HeapValue *) nan;
  }

  if (isInfinity(lhs) && isInfinity(rhs)) {
    NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
    nan->info = NAN_INFO;
    return (HeapValue *) nan;
  }

  if (isInfinity(lhs) && isLiteral(rhs)) {
    Infinity *inf = (Infinity *) sahara_alloc_no_gc(vm, sizeof(Infinity));
    inf->info = INFINITY_INFO;
    // TODO: The direction is buggy. Zero needs to be signed and the direction
    //       determined by looking at both the dividend and divisor.
    inf->direction = ((Infinity *)lhs)->direction;
    return (HeapValue *) inf;
  }

  if (isLiteral(lhs) && isInfinity(rhs)) {
    HeapNumber *lit =
      (HeapNumber *) sahara_alloc_no_gc(vm, sizeof(HeapNumber));
    lit->info = NUMBER_INFO;
    // TODO: The direction is buggy. See note above.
    lit->value = 0;
    return (HeapValue *) lit;
  }

  if (isZero(lhs)) {
    if (isZero(rhs)) {
      NaN *nan = (NaN *) sahara_alloc_no_gc(vm, sizeof(NaN));
      nan->info = NAN_INFO;
      return (HeapValue *) nan;
    } else {
      HeapNumber *lit =
        (HeapNumber *) sahara_alloc_no_gc(vm, sizeof(HeapNumber));
      lit->info = NUMBER_INFO;
      // TODO: The direction is buggy. See note above.
      lit->value = 0;
      return (HeapValue *) lit;
    }
  }

  if (isZero(rhs)) {
    // TODO: The direction is buggy. See note above.
    Infinity *inf = (Infinity *) sahara_alloc(vm, sizeof(Infinity));
    inf->info = INFINITY_INFO;
    inf->direction = NONE;
    return (HeapValue *) inf;
  }

  HeapNumber *res =
    (HeapNumber *) sahara_alloc_no_gc(vm, sizeof(HeapNumber));
  res->info = NUMBER_INFO;
  res->value = ((HeapNumber *) lhs)->value / ((HeapNumber *) rhs)->value;
  return (HeapValue *) res;
}

void
interpret_binop(VM *vm, char op)
{
  
  debugBC("Binop");
  HeapValue *rhs = toNumber(vm, (HeapValue *) pop(vm));
  HeapValue *lhs = toNumber(vm, (HeapValue *) pop(vm));
  
  HeapValue *res;
  switch (op) {
  case '+': {res = add(vm, lhs, rhs);      break;}
  case '-': {res = subtract(vm, lhs, rhs); break;}
  case '*': {res = multiply(vm, lhs, rhs); break;}
  case '/': {res = divide(vm, lhs, rhs);   break;}
  default:  {debugBC("Unkown binary operator (%c)", op); exit(1); }
  }
  push(vm, (HeapValue *) res);
}


// es: 7.2.13
HeapValue *
relational_comparison(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
  // TODO: Need to fix this as more types are added. This only
  //       addresses case #4 in the ECMAscript specification

  if (isNaN(lhs) || isNaN(rhs)) {
    Undefined *undefined = (Undefined *) sahara_alloc_no_gc(vm, sizeof(Undefined));
    undefined->info = UNDEFINED_INFO;
    return (HeapValue *) undefined;
  }
  if (isPInfinity(lhs) || isNInfinity(rhs)) {
    HeapBool *hBool = (HeapBool *) sahara_alloc_no_gc(vm, sizeof(HeapBool));
    hBool->info = BOOL_INFO;
    hBool->hBool = 0;
    return (HeapValue *) hBool;
  }
  if (isNInfinity(lhs) || isPInfinity(rhs)) {
    HeapBool *hBool = (HeapBool *) sahara_alloc_no_gc(vm, sizeof(HeapBool));
    hBool->info = BOOL_INFO;
    hBool->hBool = 1;
    return (HeapValue *) hBool;
  }
  return NULL;
}

HeapValue *
less_than(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
  HeapValue *lhs_ = toNumber(vm, lhs);
  HeapValue *rhs_ = toNumber(vm, rhs);
  
  HeapValue *comp = relational_comparison(vm, lhs_, rhs_);

  if (comp == NULL) {
    HeapBool *hBool = (HeapBool *) sahara_alloc_no_gc(vm, sizeof(HeapBool));
    hBool->info = BOOL_INFO;
    hBool->hBool =
      ((HeapNumber *) lhs_)->value <  ((HeapNumber *) rhs_)->value;
    return (HeapValue *) hBool;
  }
  return comp;
}

HeapValue *
greater_than(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
  HeapValue *lhs_ = toNumber(vm, lhs);
  HeapValue *rhs_ = toNumber(vm, rhs);
  
  HeapValue *comp = relational_comparison(vm, lhs_, rhs_);

  if (comp == NULL) {
    HeapBool *hBool = (HeapBool *) sahara_alloc_no_gc(vm, sizeof(HeapBool));
    hBool->info = BOOL_INFO;
    hBool->hBool =
      ((HeapNumber *) lhs_)->value >  ((HeapNumber *) rhs_)->value;
    return (HeapValue *) hBool;
  }
  return comp;
}

HeapValue *
less_than_equal_to(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
  HeapValue *lhs_ = toNumber(vm, lhs);
  HeapValue *rhs_ = toNumber(vm, rhs);
  
  HeapValue *comp = relational_comparison(vm, lhs_, rhs_);

  if (comp == NULL) {
    HeapBool *hBool = (HeapBool *) sahara_alloc_no_gc(vm, sizeof(HeapBool));
    hBool->info = BOOL_INFO;
    hBool->hBool =
      ((HeapNumber *) lhs_)->value <=  ((HeapNumber *) rhs_)->value;
    return (HeapValue *) hBool;
  }
  return comp;
}

HeapValue *
greater_than_equal_to(VM *vm, HeapValue *lhs, HeapValue *rhs)
{
  HeapValue *lhs_ = toNumber(vm, lhs);
  HeapValue *rhs_ = toNumber(vm, rhs);
  
  HeapValue *comp = relational_comparison(vm, lhs_, rhs_);

  if (comp == NULL) {
    HeapBool *hBool = (HeapBool *) sahara_alloc_no_gc(vm, sizeof(HeapBool));
    hBool->info = BOOL_INFO;
    hBool->hBool =
      ((HeapNumber *) lhs_)->value >=  ((HeapNumber *) rhs_)->value;
    return (HeapValue *) hBool;
  }
  return comp;
}

void
interpret_boolop(VM *vm, char op)
{
  
  debugBC("Boolop");
  HeapValue *rhs = pop(vm);
  HeapValue *lhs = pop(vm);
  HeapValue *res;
  switch (op) {
  case '<': {res = less_than(vm, lhs, rhs);             break;}
  case '>': {res = greater_than(vm, lhs, rhs);          break;}
  case 64:  {res = less_than_equal_to(vm, lhs, rhs);    break;}
  case 65:  {res = greater_than_equal_to(vm, lhs, rhs); break;}
  }
  push(vm, res);
}

// es: 20.1.2.2
void /* bool */
isFinite(VM *vm /* number */)
{
  ActivationFrame *af = vm->af;

  HeapNamedValue *num = af->frame[0];
  HeapBool *result = (HeapBool *) sahara_alloc(vm, sizeof(HeapBool));
  result->info = BOOL_INFO;
  switch (num->val->info) {
  case NUMBER_INFO: {
    result->hBool = true;
    break;
  }
  default: {
    result->hBool = false;
  }
  }
  push(vm, (HeapValue *) result);
}

// es: 20.1.1
void /* Number Object */
number_constructor(VM *vm /* value */)
{
  ActivationFrame *af = vm->af;
  
  HeapNamedValue *value = af->frame[0];
  HeapValue *value_;
  if (value == NULL) {
    HeapNumber *val = (HeapNumber *) sahara_alloc(vm, sizeof(HeapNumber));
    val->info = NUMBER_INFO;
    val->value = 0;
    value_ = (HeapValue *) val;
  } else {
    switch (value->val->info) {
    case NUMBER_INFO: {
      value_ = value->val;
      break;
    }
    case UNDEFINED_INFO: {
      NaN *nan = (NaN *) sahara_alloc(vm, sizeof(NaN));
      nan->info = NAN_INFO;
      value_ = (HeapValue *) nan;
      break;
    }
    default: {
      debugBC("Need to cast value to number...");
      exit(1);
    }
    }
  }

  int propertyCount = 1;
  
  NumberObject *obj =
    (NumberObject *) sahara_alloc(vm, sizeof(NumberObject) +
                                  (sizeof(ObjectProperty) * propertyCount));
  obj->info = NUMBER_OBJECT_INFO;
  obj->number = value_;
  obj->propertyCount = 1;
  
  ESObject *numberConstructor =
    (ESObject *) lookupMember(vm, vm->globalObject, "Number");

  ObjectProperty *proto =
    makeValueProperty(vm,
                      "__proto__",
                      (HeapValue *) numberConstructor,
                      false, false, false);
  
  obj->properties[0] = proto;

  push(vm, (HeapValue *) obj);
} 
