#include <string.h>

#include "object_util.h"

ObjectProperty *
makeConstructorProperty(VM *vm,
                        char *name,
                        ESObject *obj,
                        bool writable,
                        bool enumerable,
                        bool configurable)
{
  int keyLen = strlen(name);
  
  ObjectProperty *prop =
    (ObjectProperty *) sahara_alloc(vm, sizeof(ObjectProperty));
  ConstructorProperty *constructorProperty =
    (ConstructorProperty *) sahara_alloc(vm, sizeof(ConstructorProperty));
  HeapID *key = (HeapID *) sahara_alloc(vm, sizeof(HeapID) + keyLen);

  constructorProperty->info         = CONSTRUCTOR_PROPERTY_INFO;
  constructorProperty->value        = obj;
  constructorProperty->writable     = writable;
  constructorProperty->enumerable   = enumerable;
  constructorProperty->configurable = configurable;

  key->info = ID_INFO;
  key->size = keyLen;
  memcpy(&key->id, name, keyLen);

  prop->info  = OBJECT_PROPERTY_INFO;
  prop->key   = key;
  prop->value = (Property *) constructorProperty;

  return prop;
}

ObjectProperty *
makeValueProperty(VM *vm,
                  char *name,
                  HeapValue *value,
                  bool writable,
                  bool enumerable,
                  bool configurable)
{
  int keyLen = strlen(name);
  
  ObjectProperty *prop =
    (ObjectProperty *) sahara_alloc(vm, sizeof(ObjectProperty));
  DataProperty *dataProperty =
    (DataProperty *) sahara_alloc(vm, sizeof(DataProperty));
  HeapID *key = (HeapID *) sahara_alloc(vm, sizeof(HeapID) + keyLen);

  dataProperty->info         = DATA_PROPERTY_INFO;
  dataProperty->value        = value;
  dataProperty->writable     = writable;
  dataProperty->enumerable   = enumerable;
  dataProperty->configurable = configurable;

  key->info = ID_INFO;
  key->size = keyLen;
  memcpy(&key->id, name, keyLen);

  prop->info  = OBJECT_PROPERTY_INFO;
  prop->key   = key;
  prop->value = (Property *) dataProperty;

  return prop;
}
