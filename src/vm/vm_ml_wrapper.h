#ifndef VM_ML_WRAPPER_H
#define VM_ML_WRAPPER_H

#include <caml/mlvalues.h>

value initializeVM ();
void  cleanupVM    (value vm);
value interpretVM  (value vm);

#endif
