#ifndef DEBUG_H
#define DEBUG_H

#include <stdio.h>
#include <stdarg.h>

#define BC 0

enum { LOG_BC, LOG_GC };

void
debug(int system, const char *file, int line, const char *fmt, ...);

#ifdef DEBUG
#define debugBC(...) debug(LOG_BC, __FILE__, __LINE__, __VA_ARGS__)
#define debugGC(...) debug(LOG_GC, __FILE__, __LINE__, __VA_ARGS__)
#else
#define debugBC(...) (void) 0
#define debugGC(...) (void) 0
#endif

#endif
