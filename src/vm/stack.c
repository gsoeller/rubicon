#include <stdlib.h>
#include <stdio.h>

#include "stack.h"
#include "debug.h"

void
save_ret(VM *vm, HeapValue *hv)
{
  vm->ret_register = hv;
}

void
push(VM *vm, HeapValue *item)
{
  switch (item->info) {
  case NUMBER_INFO: {
    debugBC("Pushing number: %f", ((HeapNumber *)item)->value);
    break;
  }
  case ID_INFO: {
    HeapID *hi = (HeapID *) item;
    char *id = hi->id;
    debugBC("Pushing id: %s", id);
    break;
  }
  case HEAP_NAMED_VALUE_INFO: {
    HeapNamedValue *val = (HeapNamedValue *)item;
    char *id = val->id->id;
    debugBC("Pushing named value: %s", id);
    break;
  }
  case CODE_INFO: {
    debugBC("Pushing code");
    break;
  }
  default: {
      debugBC("Pushing something else %d", item->info);
    }
  }
  slot *top  = malloc(sizeof(slot));
  top->item = item;
  top->previous = vm->stack;
  vm->stack = top;
  save_ret(vm, item);
}

/*
  Caller is responsible for freeing slots
 */
HeapValue *
pop(VM *vm)
{
  if (vm->stack == NULL && vm->ret_register != NULL) {
    return vm->ret_register;
  }
  slot *top = vm->stack;
  vm->stack = top->previous;
  switch (top->item->info) {
  case NUMBER_INFO: {
    debugBC("Popping number: %f", ((HeapNumber *)top->item)->value);
    break;
  }
  case ID_INFO: {
    HeapID *hi = (HeapID *) top->item;
    char *id = hi->id;
    debugBC("Popping id: %s", id);
    break;
  }
  case HEAP_NAMED_VALUE_INFO: {
    HeapNamedValue *val = (HeapNamedValue *)top->item;
    char *id = val->id->id;
    debugBC("Popping named value: %s", id);
    break;
  }
  case SLOT_SAVE_INFO: {
    debugBC("Popping slot save");
    break;
  }
  default: {
      debugBC("Popping something else %d", top->item->info);
    }
  }
  HeapValue *val = top->item;
  free(top);
  return val;
}

void
push_context(VM *vm)
{
  SlotSave *save = (SlotSave *) sahara_alloc(vm, sizeof(SlotSave));
  save->info = SLOT_SAVE_INFO;
  slot *top  = malloc(sizeof(slot));
  top->item = (HeapValue *) save;
  top->previous = vm->stack;
  vm->stack = top;
}

void
pop_context(VM *vm)
{
  while(vm->stack->item->info != SLOT_SAVE_INFO) {
    pop(vm);
  }
  pop(vm);
}
