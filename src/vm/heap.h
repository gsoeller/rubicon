#ifndef HEAP_H
#define HEAP_H

#include "object.h"
#include "vm.h"

VM *
initializeHeap();

void
cleanup(VM *vm);

int
sizeofHeapValue(HeapValue *hv);

ActivationFrame *
allocActivationFrame(VM *vm, int numLocals);

HeapNamedValue *
find_var(char *id, ActivationFrame *ctx);

HeapValue *
sahara_alloc(VM *vm, int required);

HeapValue *
sahara_alloc_no_gc(VM *vm, int required);

HeapValue *
lookupMember(VM *vm, ESObject *base, char *property);

#endif
