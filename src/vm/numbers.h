#ifndef NUMBERS_H
#define NUMBERS_H

void interpret_binop(VM *vm, char op);
void interpret_boolop(VM *vm, char op);

void isFinite(VM *vm);
void number_constructor(VM *vm);

#endif
