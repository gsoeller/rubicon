#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>

#include "heap.h"
#include "opcodes.h"
#include "debug.h"

void emit_bc_(VM *vm, value code);

void
emit_(VM *vm, void *data, int size)
{
  //printf("Emitting %d %d %p\n", *(int *)data, size, hp);
  //printf("Emitting (double) %f %d\n", *(double *)data, size);
  memcpy(vm->code_hp, data, size);
  vm->code_hp += size;
  if (vm->code_heap + 4096 > vm->code_hp) {
    //printf("We dun fuked up\n");
    //exit(1);
  }
}

void
emit(VM *vm, int data)
{
  emit_(vm, &data, OPCODE_SIZE);
}

void emit__(VM *vm, int data, int size)
{
  emit_(vm, &data, size);
}


#define JSValueTag         0
#define CodeTag            1
#define ActivationFrameTag 2

#define UndefinedTag 0
#define NullTag      1
#define ObjectTag    2
#define BooleanTag   0
#define StringTag    1
#define SymbolTag    2
#define NumberTag    3
#define PInfinityTag 0
#define NInfinityTag 1
#define NaNTag       2

void
emit_js_value(VM *vm, value val)
{
  CAMLparam1(val);
  
  if (Is_long(val)){
    switch (Int_val(val)) {
    case UndefinedTag: debugBC("Undefined"); emit(vm, UNDEFINED_VAL); break;
    case NullTag: debugBC("Null"); emit(vm, NULL_VAL); break;
    case ObjectTag: debugBC("Objects not implemented... Exiting."); exit(1);
    default: debugBC("Error"); break;
    }
  } else {
    switch (Tag_val(val)) {
    case NumberTag: {
      if(Is_long(Field(val, 0))) {
        switch (Int_val(Field(val, 0))) {
        case PInfinityTag: debugBC("PInfinity"); emit(vm, PINFINITY_VAL); break;
        case NInfinityTag: debugBC("NInfinity"); emit(vm, NINFINITY_VAL); break;
        case NaNTag: debugBC("NaN"); emit(vm, NAN_VAL); break;
        }
      } else {
        debugBC("Number: %f", Double_val(Field(Field(val, 0), 0)));
        emit(vm, NUMBER_VAL);
        double *lit = &Double_val(Field(Field(val,0), 0));
        debugBC("cesr: %f", *lit);
        emit_(vm, lit, sizeof(double));
      }
      break;
    }
    case StringTag: {
      debugBC("String: %s", String_val(Field(val, 0)));
      emit(vm, STRING_VAL);
      char *id = String_val(Field(val,0));
      int s = caml_string_length(Field(val,0));
      debugBC("%d", s);
      emit_(vm, (void *)id, s);
      break;
    }
    case BooleanTag: {
      debugBC("Bool: %d", Bool_val(Field(val, 0)));
      emit(vm, BOOL_VAL);
      int hBool = Bool_val(Field(val, 0));
      emit_(vm, &hBool, sizeof(int));
      break;
    }
    case SymbolTag: {
      debugBC("Symbols not implemented.... Exiting.");
      exit(1);
    }
    }
  }
}

void
emit_stack_value(VM *vm, value val)
{
  CAMLparam1(val);
  CAMLlocal3(head, code_list, body);
  
  if (Is_block(val)) {
    switch (Tag_val(val)) {
    case JSValueTag: {
      body = Field(val, 0);
      emit_js_value(vm, body);
      break;
    }
    case CodeTag: {
      debugBC("Code");
      emit(vm, CODE_VAL);
      int *ptrToEnd = vm->code_hp;
      emit__(vm, UNDEFINED_TMP, sizeof(int *));
      code_list = Field(val,0);
      debugBC("Got list %p", ptrToEnd);
      while (code_list != Val_emptylist)
        {
          debugBC("iter %p", vm->code_hp);
          head = Field(code_list, 0);
          //printf("tag %d\n", Tag_val(head));
          emit_bc_(vm, head);
          code_list = Field(code_list, 1);
        }
      emit(vm, CODE_END);
      int **code_hp_ = vm->code_hp;
      memcpy(ptrToEnd,  &vm->code_hp, sizeof(int *));
      int **p = (int **)ptrToEnd;
      debugBC("PTR: %p %p %p", ptrToEnd, *p, vm->code_hp);
      break;
    }
    case ActivationFrameTag: {
      debugBC("Activation frame.. Not implemented");
      exit(1);
      break;
    }
    }
  } else {
    debugBC("Expected block.... Exiting.");
    exit(1);
  }
}

void
emit_bc_(VM *vm, value code)
{
  CAMLparam1(code);

  //bc opcode;
  //opcode = Field(bc, 0);
  //int bccc = Int_val(opcode);
  if (Is_long(code)) {
    switch (Int_val(code)) {
    case 0: debugBC("Ret"); emit(vm, RET); break;
    case 1: debugBC("Plus"); emit(vm, PLUS); break;
    case 2: debugBC("Multiply"); emit(vm, MULTIPLY); break;
    case 3: debugBC("Minus"); emit(vm, MINUS); break;
    case 4: debugBC("Divide"); emit(vm, DIVIDE); break;
    case 5: debugBC("GT"); emit(vm, GT); break;
    case 6: debugBC("LT"); emit(vm, LT); break;
    case 7: debugBC("LTE"); emit(vm, LTE); break;
    case 8: debugBC("GTE"); emit(vm, GTE); break;
    case 9:
      debugBC("Activation alloc local");
      emit(vm, ACTIVATION_ALLOC_LOCAL); break;
    case 10:
      debugBC("Activation push");
      emit(vm, ACTIVATION_PUSH); break;
    case 11:
      debugBC("Activation push arg");
      emit(vm, ACTIVATION_PUSH_ARG); break;
    case 12:
      debugBC("Activation name arg");
      emit(vm, ACTIVATION_NAME_ARG); break;
    case 13:
      debugBC("Activation store");
      emit(vm, ACTIVATION_STORE); break;
    case 14:
      debugBC("Activation load");
      emit(vm, ACTIVATION_LOAD); break;
    case 15:
      debugBC("Activation create");
      emit(vm, ACTIVATION_CREATE); break;
    case 16:
      debugBC("Activation switch");
      emit(vm, ACTIVATION_SWITCH); break;
    case 17:
      debugBC("Activation switch constructor");
      emit(vm, ACTIVATION_SWITCH_CONSTRUCTOR);
    case 18:
      debugBC("Activation revert");
      emit(vm, ACTIVATION_REVERT); break;
    case 19:
      debugBC("Bind");
      emit(vm, BIND); break;
    case 20:
      debugBC("Jmp true");
      emit(vm, JMP_TRUE); break;
    case 21:
      debugBC("While cond");
      emit(vm, WHILE_COND); break;
    case 22:
      debugBC("Jmp while true");
      emit(vm, JMP_WHILE_TRUE); break;
    case 23:
      debugBC("End body");
      emit(vm, END_BODY); break;
    case 24:
      debugBC("Jump top");
      emit(vm, JMP_TOP); break;
    case 25:
      debugBC("Load member");
      emit(vm, LOAD_MEMBER); break;
    }
    
  } else {
    switch (Tag_val (code)) {
    case 0: {
      debugBC("Push");
      emit(vm, PUSH);
      emit_stack_value(vm, Field(code,0));
      break;
    }
    case 1: {
      debugBC("Label: %s", String_val(Field(code,0)));
      emit(vm, LABEL);
      char *label = String_val(Field(code,0));
      int s = caml_string_length(Field(code,0));
      debugBC("%d", s);
      emit_(vm, (void *)label, s);
      emit_bc_(vm, Field(code,1));
      break;
    }
      default: debugBC("Unknown tag %d", Tag_val(code)); exit(1);
    }
  }
  CAMLreturn0;
}


void
emit_bc(value vm, value code)
{
  CAMLparam2(vm, code);
  emit_bc_((VM *) vm, code);
  CAMLreturn0;
}
