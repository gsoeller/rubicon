#ifndef SAHARA_EMITTER_H
#define SAHARA_EMITTER_H

#include <caml/mlvalues.h>

typedef enum bc_ {
                  RET,
                  PLUS,
                  MULTIPLY,
                  MINUS,
                  DIVIDE,
                  GREATER_THAN,
                  LESS_THAN,
                  LESS_THAN_EQUAL_TO,
                  GREATER_THAN_EQUAL_TO,
                  PUSH,
                  ACTIVATION_ALLOC_LOCAL,
                  ACTIVATION_PUSH,
                  ACTIVATION_PUSH_ARG,
                  ACTIVATION_STORE,
                  ACTIVATION_LOAD,
                  ACTIVATION_CREATE,
                  ACTIVATION_SWITCH,
                  ACTIVATION_SWITCH_CONSTRUCTOR,
                  ACTIVATION_REVERT,
                  BIND,
                  LABEL,
                  JMP_TRUE,
                  WHILE_COND,
                  JMP_WHILE_TRUE,
                  END_BODY,
                  JMP_TOP,
                  LOAD_MEMBER
} bc;

void emit_bc(value vm, value bc);

#endif
