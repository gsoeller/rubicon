#include <string.h>
#include <math.h>

#include "object.h"
#include "vm.h"
#include "heap.h"
#include "debug.h"
#include "numbers.h"
#include "object_util.h"

// es: 20.1.1
ESObject *
makeNumberObject(VM *vm)
{
  int numProperties = 16; // 9 values + 6 functions + 1 prototype
  int constructorSize =
    sizeof(ESObject) + (sizeof(ObjectProperty) * numProperties);
  ESObject *numberObject = (ESObject *) sahara_alloc(vm, constructorSize);
  numberObject->info = OBJECT_INFO;
  
  HeapNumber *epsilon = (HeapNumber *) sahara_alloc(vm, sizeof(HeapNumber));
  epsilon->info  = NUMBER_INFO;
  epsilon->value = pow(2, -52);

  HeapNumber *length = (HeapNumber *) sahara_alloc(vm, sizeof(HeapNumber));
  length->info  = NUMBER_INFO;
  length->value = 1;

  NativeCode *isFiniteNative =
    (NativeCode *) sahara_alloc(vm, sizeof(NativeCode));
  isFiniteNative->info = NATIVE_CODE_INFO;
  char *isFiniteName = "isFinite";
  HeapID *isFiniteID =
    (HeapID *) sahara_alloc(vm, sizeof(HeapID) + strlen(isFiniteName));
  isFiniteID->info = ID_INFO;
  isFiniteID->size = strlen(isFiniteName);
  isFiniteNative->name = isFiniteID;
  isFiniteNative->num_locals = 1;
  isFiniteNative->code = isFinite;

  NativeCode *constructor =
    (NativeCode *) sahara_alloc(vm, sizeof(NativeCode));
  constructor->info = NATIVE_CODE_INFO;
  char *constructorName = "constructor";
  HeapID *constructorID =
    (HeapID *) sahara_alloc(vm, sizeof(HeapID) + strlen(constructorName));
  constructorID->info = ID_INFO;
  constructorID->size = strlen(constructorName);
  constructor->name = constructorID;
  constructor->num_locals = 1;
  constructor->code = number_constructor;

  
  ObjectProperty *epsilonProperty =
    makeValueProperty(vm,
                      "EPSILON",
                      (HeapValue *) epsilon, false, false, false);
  ObjectProperty *isFiniteProperty =
    makeValueProperty(vm,
                      isFiniteName,
                      (HeapValue *) isFiniteNative, false, false, false);
  ObjectProperty *lengthProperty =
    makeValueProperty(vm,
                      "length",
                      (HeapValue *) length, false, false, true);
  ObjectProperty *constructorProperty =
    makeValueProperty(vm,
                      constructorName,
                      (HeapValue *) constructor, false, false, false);
  
  numberObject->properties[0] = epsilonProperty;
  numberObject->properties[1] = isFiniteProperty;
  numberObject->properties[2] = lengthProperty;
  numberObject->properties[3] = constructorProperty;

  numberObject->propertyCount = 4;
  
  return numberObject;
  
  /* Value properties
     1. EPSILON
     2. MAX_SAFE_INTEGER
     3. MAX_VALUE
     4. MIN_SAFE_INTEGER
     5. MIN_VALUE
     6. NaN
     7. NEGATIVE_INFINITY
     8. POSITIVE_INFINITY
     9. prototype

     Function properties
     1. isFinite
     2. isInteger
     3. isNaN
     4. isSafeInteger
     5. parseFloat
     6. parseInt
*/
}


void
addValuePropertyToGlobalScope(VM *vm, char *name, HeapValue *value)
{
  if (vm->af->af_pointer >= vm->af->num_locals) {
    debugBC("Not enough space on global activation frame");
    
  }

  int nameLen = strlen(name);
  HeapID *id = (HeapID *) sahara_alloc(vm, sizeof(HeapID) + nameLen);

  id->info = ID_INFO;
  id->size = nameLen;
  memcpy(&id->id, name, nameLen);

  HeapNamedValue *nv =
    (HeapNamedValue *) sahara_alloc(vm, sizeof(HeapNamedValue));
  
  nv->info = HEAP_NAMED_VALUE_INFO;
  nv->id   = id;
  nv->val  = value;

  vm->af->frame[vm->af->af_pointer] = nv;
  vm->af->af_pointer++;
  vm->af->name_pointer++;
}

// es: 18
ESObject *
makeGlobalObject(VM *vm)
{
  ESObject *proto = (ESObject *) sahara_alloc(vm, sizeof(ESObject));
  proto->info = UNDEFINED_INFO;
  proto->propertyCount = 0;

  // es:18.1
  int numValueProperties = 3;
  
  Infinity *infinityValue   = (Infinity *)  sahara_alloc(vm, sizeof(Infinity));
  NaN *nanValue             = (NaN *)       sahara_alloc(vm, sizeof(NaN));
  Undefined *undefinedValue = (Undefined *) sahara_alloc(vm, sizeof(Undefined));

  infinityValue->info  = INFINITY_INFO;
  nanValue->info       = NAN_INFO;
  undefinedValue->info = UNDEFINED_INFO;


  ObjectProperty *infinity =
    makeValueProperty(vm,
                      "infinity",
                      (HeapValue *) infinityValue,
                      false, false, false);
  ObjectProperty *nan =
    makeValueProperty(vm,
                      "NaN",
                      (HeapValue *) nanValue,
                      false, false, false);
  ObjectProperty *undefined =
    makeValueProperty(vm,
                      "undefined",
                      (HeapValue *) undefinedValue,
                      false, false, false);

  addValuePropertyToGlobalScope(vm, "infinity",  (HeapValue *) infinityValue);
  addValuePropertyToGlobalScope(vm, "NaN",       (HeapValue *) nanValue);
  addValuePropertyToGlobalScope(vm, "undefined", (HeapValue *) undefinedValue);
  
  // TODO: es:18.2
  int numFunctionProperties = 0;

  // TODO: es:18.3
  int numConstruxtorProperties = 1;

  ESObject *numberObject = makeNumberObject(vm);
  ObjectProperty *numberProperty =
    makeConstructorProperty(vm,
                      "Number",
                      numberObject,
                      false, false, false);

  addValuePropertyToGlobalScope(vm, "Number",  (HeapValue *) numberObject);
  
  int numProperties =
    numValueProperties + numFunctionProperties + numConstruxtorProperties;

  int globalObjectSize = sizeof(ESObject) +
    (sizeof(ObjectProperty *) * numProperties);
  ESObject *globalObject = (ESObject *) sahara_alloc(vm, globalObjectSize);

  globalObject->info          = OBJECT_INFO;
  globalObject->propertyCount = numProperties;

  globalObject->properties[0] = nan;
  globalObject->properties[1] = infinity;
  globalObject->properties[2] = undefined;
  
  globalObject->properties[3] = numberProperty;

  return globalObject;
}
