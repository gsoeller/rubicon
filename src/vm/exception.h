#include <caml/fail.h>
#include <caml/callback.h>

void
raise_reference_error(char *name)
{
  caml_raise_with_string(*caml_named_value("reference error"), name);
}

void
raise_af_oom_error()
{
  caml_raise_constant(*caml_named_value("activation oom"));
}
