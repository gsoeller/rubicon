#ifndef OBJECT_H
#define OBJECT_H

#include <stdbool.h>

// ECMAScript types
#define UNDEFINED_INFO         1
#define NULL_INFO              2
#define BOOL_INFO              3
#define STRING_INFO            4
#define SYMBOL_INFO            5
#define NUMBER_INFO            6
#define OBJECT_INFO            7

// Specific ECMAScript values
#define NAN_INFO               8
#define INFINITY_INFO          9

// Sahara internals
#define ID_INFO                     10
#define CODE_INFO                   11
#define HEAP_NAMED_VALUE_INFO       12
#define ACTIVATION_FRAME_INFO       13
#define SLOT_SAVE_INFO              14
#define IP_INFO                     15
#define DATA_PROPERTY_INFO          16
#define ACCESSOR_PROPERTY_INFO      17
#define OBJECT_PROPERTY_INFO        18
#define PROPERTY_INFO               19
#define CONSTRUCTOR_PROPERTY_INFO   20
#define NATIVE_CODE_INFO            21
#define OBJECT_CODE_INFO            22

// ECMAScript object types
//#define OBJECT_OBJECT_INFO     23
#define FUNCTION_OBJECT_INFO   24
#define BOOLEAN_OBJECT_INFO    25
#define SYMBOL_OBJECT_INFO     26
#define ERROR_OBJECT_INFO      27
#define NUMBER_OBJECT_INFO     28

// Direction
#define NONE     0
#define POSITIVE 1
#define NEGATIVE 2

typedef int ObjInfo;
typedef int Direction;

typedef void * GCWord;

typedef struct HeapValue_ {
  ObjInfo info;
  GCWord gcword;
} HeapValue;

typedef struct HeapNumber_ {
  ObjInfo info;
  GCWord gcword;
  double value;
} HeapNumber;

typedef struct HeapID_ {
  ObjInfo info;
  GCWord gcword;
  int size;
  char id[];
} HeapID;

typedef struct HeapBool_ {
  ObjInfo info;
  GCWord gcword;
  int hBool;
} HeapBool;

typedef struct HeapCode_ {
  ObjInfo info;
  GCWord gcword;
  HeapID *name;
  int num_locals;
  int *code;
} HeapCode;

struct VM_;
typedef void (*nativeFun) (struct VM_ *);

typedef struct NativeCode_ {
  ObjInfo info;
  GCWord gcword;
  HeapID *name;
  int num_locals;
  nativeFun code;
} NativeCode;

typedef struct HeapNamedValue_ {
  ObjInfo info;
  GCWord gcword;
  HeapID *id;
  HeapValue *val;
} HeapNamedValue;

typedef struct ActivationFrame_ {
  ObjInfo info;
  // Native or JS. Tells how to cast the instruction pointer
  ObjInfo codeType;
  GCWord gcword;
  void *ip;
  struct ActivationFrame_ *staticLink;
  // Number of variables in the frame---locals + args
  int num_locals;
  // Remembers the location to store the next variable
  int af_pointer;
  // Remembers the location to name the next variable
  int name_pointer;
  // Pointers to all variables in frame
  HeapNamedValue *frame[];
} ActivationFrame;

typedef struct Infinity_ {
  ObjInfo info;
  Direction direction;
  GCWord gcword;
} Infinity;

typedef struct NaN_ {
  ObjInfo info;
  GCWord gcword;
} NaN;

typedef struct Undefined_ {
  ObjInfo info;
  GCWord gcword;
} Undefined;

typedef struct SlotSave_ {
  ObjInfo info;
  GCWord gcword;
} SlotSave;

typedef struct IP_ {
  ObjInfo info;
  GCWord gcword;
  void *ip;
} IP;

// Defines the type of property: data or accessor
typedef int PropertyInfo;

// es:6.1.7.1
typedef struct DataProperty_ {
  ObjInfo info;
  GCWord gcword;
  HeapValue *value;
  bool writable;
  bool enumerable;
  bool configurable;
} DataProperty;

// es:6.1.7.1
typedef struct AccessorProperty_ {
  ObjInfo info;
  GCWord gcword;
  HeapValue *get;
  HeapValue *set;
  bool writable;
  bool enumerable;
  bool configurable;
} AccessorProperty;

struct ESObject_;

typedef struct ConstructorProperty_ {
  ObjInfo info;
  GCWord gcword;
  struct ESObject_ *value;
  HeapValue *get;
  HeapValue *set;
  bool writable;
  bool enumerable;
  bool configurable;
} ConstructorProperty;

typedef struct Property_ {
  ObjInfo info;
  GCWord gcword;
} Property;

typedef struct ObjectProperty_ {
  ObjInfo info;
  GCWord gcword;
  HeapID *key;
  Property *value;
} ObjectProperty;

typedef struct NumberObject_ {
  ObjInfo info;
  HeapValue *number;
  int propertyCount;
  ObjectProperty *properties[];
} NumberObject;

// es:6.1.7
typedef struct ESObject_ {
  ObjInfo info;
  GCWord gcword;
  int propertyCount;
  ObjectProperty *properties[];
} ESObject;


typedef struct slot_ {
  HeapValue *item;
  struct slot_ *previous;
} slot;


typedef struct VM_ {
  void *heap;
  void *hp;
  void *code_heap;
  void *code_hp;
  void *to_space;
  void *tp; // to_space pointer
  ActivationFrame *af;

  // Stack machine
  HeapValue *ret_register;
  HeapValue *intermediate_register;
  slot *stack;

  // GC
  void *scan_ptr;
  void *copy_ptr;
  bool ready_to_gc;

  // ECMAscript
  ESObject *globalObject;
} VM;

ESObject *
makeNumberObject(VM *vm);

ESObject *
makeGlobalObject(VM *vm);

#endif
