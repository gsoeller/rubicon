#include <caml/mlvalues.h>
#include <caml/memory.h>
#include <caml/alloc.h>
#include <caml/custom.h>

#include "vm_ml_wrapper.h"
#include "sahara.h"
#include "opcodes.h"
#include "debug.h"

value
initializeVM()
{
  CAMLparam0 ();
  VM *vm = initializeHeap();
  return (value) vm;
}

void
cleanupVM(value vm)
{
  CAMLparam1 (vm);
  cleanup((VM *) vm);
  CAMLreturn0;
}

value
interpret_end(VM *vm, HeapValue *res)
{
  CAMLparam0();
  CAMLlocal3 (result, result2, wrap);
  
  HeapValue *hv = res;

  while(1) {
    switch (hv->info) {
    case NUMBER_INFO: {
      double val = ((HeapNumber *) hv)->value;
      wrap = caml_copy_double(val);
      result = caml_alloc(1, RETURN_REAL);
      Store_field (result, 0, wrap);

      result2 = caml_alloc(1, RETURN_NUMBER);
      Store_field(result2, 0, result);
      
      CAMLreturn(result2);
    }
    case ID_INFO: {
      HeapID *id = (HeapID *) hv;
      char *id_ = id->id;
      HeapNamedValue *nv = find_var(id_, vm->af);
      hv = nv->val;
      break;
    }
    case BOOL_INFO: {
      HeapBool *hBool = (HeapBool *) hv;
      wrap = Val_bool(hBool->hBool);
      result = caml_alloc(1, RETURN_BOOLEAN);
      Store_field (result, 0, wrap);
      CAMLreturn(result);
    }
    case UNDEFINED_INFO: {
      CAMLreturn(Val_int(RETURN_UNDEFINED));
    }
    case HEAP_NAMED_VALUE_INFO: {
      hv = ((HeapNamedValue *) hv)->val;
      break;
    }
    case NAN_INFO: {
      result = caml_alloc(1, RETURN_NUMBER);
      Store_field(result, 0, Val_int(RETURN_NAN));    
      CAMLreturn(result);
    }
    case NUMBER_OBJECT_INFO: {
      double val = ((HeapNumber *)((NumberObject *) hv)->number)->value;
      wrap = caml_copy_double(val);
      result = caml_alloc(1, RETURN_REAL);
      Store_field (result, 0, wrap);

      result2 = caml_alloc(1, RETURN_NUMBER);
      Store_field(result2, 0, result);
      
      CAMLreturn(result2);
    }
    default: {
      debugBC("Return type not implemented yet (%d)", res->info);
      exit(1);
    }
    }
  }
}

value
interpretVM(value vm)
{
  CAMLparam1 (vm);
  
  HeapValue *res = interpret((VM *) vm);
  value res_ = interpret_end((VM *) vm, res);
  
  CAMLreturn(res_);
}
