#include "object.h"
#include "vm.h"

ObjectProperty *
makeConstructorProperty(VM *vm,
                        char *name,
                        ESObject *obj,
                        bool writable,
                        bool enumerable,
                        bool configurable);

ObjectProperty *
makeValueProperty(VM *vm,
                  char *name,
                  HeapValue *value,
                  bool writable,
                  bool enumerable,
                  bool configurable);
