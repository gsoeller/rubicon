
(* Variable name *)
type id = string

(* Position on the stack *)
type activation_frame_offset = int

type next_offset = int

type venv =
  | VEnv of (id, activation_frame_offset) Hashtbl.t

type env =
  | Empty
  | Env of next_offset ref * venv * static_link
and static_link = env
                
val create_empty_env : unit -> env
val create_env : env -> env
val count_locals : env -> int
  
val lookup : env -> id -> activation_frame_offset option

val insert : env -> id -> activation_frame_offset
