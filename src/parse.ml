
type program' = (Loc.t * (Loc.t * (Loc.t, Loc.t) Flow_ast.Statement.t') list *
                   (Loc.t * Flow_ast.Comment.t') list)
                  [@@deriving show]
                  
type program = program' * (Loc.t * Parse_error.t) list
                                                  
let parse file =
  let content = Core.In_channel.read_all file in
  Parser_flow.program_file content (Some (File_key.SourceFile file));;
