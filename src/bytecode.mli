open Parser_flow
open Vm
       
type t
      
val generate_bytecode : (Loc.t, Loc.t) Ast.program -> t
val emit_bytecode : vm -> t -> unit
                                                        
val show : t -> string

module type Interpreter =
sig
  val interpret : vm -> t -> js_type
  val exit_interp : vm -> unit
end

module Sahara : Interpreter
(*val emit_bytecode : t -> unit*)
