
(* Variable name *)
type id = string

(* Position on the stack *)
type activation_frame_offset = int

type next_offset = int

type venv =
  | VEnv of (id, activation_frame_offset) Hashtbl.t

type env =
  | Empty
  | Env of next_offset ref * venv * static_link
and static_link = env
                
let create_empty_env () = Env(ref 0, VEnv (Hashtbl.create 10), Empty)
let create_env sl = Env(ref 0, VEnv (Hashtbl.create 10), sl)

let count_locals = function
  | Empty -> 0
  | Env(_,VEnv venv,_) -> Hashtbl.length venv
                  
let rec lookup (Env(_,VEnv env,sl)) id =
  match Hashtbl.mem env id with
  | true  -> Some (Hashtbl.find env id)
  | false ->
     begin match sl with
     | Empty -> None
     | env   -> lookup env id end

let insert (Env(next_offset, VEnv venv, sl)) id =
  Hashtbl.add venv id !next_offset;
  let offset = !next_offset in
  next_offset := offset + 1;
  offset
  
