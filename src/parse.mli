
type program' = (Loc.t * (Loc.t * (Loc.t, Loc.t) Flow_ast.Statement.t') list *
     (Loc.t * Flow_ast.Comment.t') list)
                  [@@deriving show]
                  
type program = program' * (Loc.t * Parse_error.t) list
                            
                            
val parse : string -> program
