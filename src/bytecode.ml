open Parser_flow
open Vm

type t = bc list [@@deriving show]


let emit_bytecode vm = function
  | bc -> Log.info "Emitting bytecode";
          List.iter (fun c -> Vm.emit_bc vm c) bc

module type Interpreter =
sig
  val interpret : vm -> t -> js_type
  val exit_interp : vm -> unit
end

module Sahara : Interpreter =
  struct

    let interpret vm bc =
      emit_bytecode vm bc;
      Vm.interpretVM vm

    let exit_interp vm = Vm.cleanupVM vm

  end
    
let bcs = ref ([] : t)

let funmap = Hashtbl.create 10

exception BytecodeError

(*let append instr = bcs := !bcs@[instr]*)
            
let rec generate_bytecode = function
  | (program : (Loc.t, Loc.t) Ast.program) ->
    let venv = Env.create_empty_env () in
    let prog = generate_program venv program in
    let prog' = prog @ [ActivationRevert] in 
    let bcs =
      [Push (JSValue (String "_enter_"))] @
        (*[Push (JSValue (Number 0.))] @*)
      [Push (Code prog')] @    
      [Bind] @
      [Push (JSValue (String "_enter_"))] @    
      [ActivationCreate] @
      [ActivationSwitch] @
      [Ret] in
     bcs

and generate_program venv = function
  | _,statements,comments -> generate_statements venv statements

and generate_statements venv = function
  | statements ->
     let blocks = List.map (generate_statement venv) statements in
     List.flatten blocks

and generate_statement venv = function
  | _,Ast.Statement.Block block -> generate_block' venv block
  | _,Ast.Statement.Break break -> raise BytecodeError
  | _,Ast.Statement.ClassDeclaration cd -> raise BytecodeError
  | _,Ast.Statement.Continue c -> raise BytecodeError
  | _,Ast.Statement.Debugger -> raise BytecodeError
  | _,Ast.Statement.DeclareClass dc -> raise BytecodeError
  | _,Ast.Statement.DeclareExportDeclaration ded -> raise BytecodeError
  | _,Ast.Statement.DeclareFunction df -> raise BytecodeError
  | _,Ast.Statement.DeclareInterface di -> raise BytecodeError
  | _,Ast.Statement.DeclareModule dm -> raise BytecodeError
  | _,Ast.Statement.DeclareModuleExports dme -> raise BytecodeError
  | _,Ast.Statement.DeclareTypeAlias dta -> raise BytecodeError
  | _,Ast.Statement.DeclareOpaqueType dot -> raise BytecodeError
  | _,Ast.Statement.DeclareVariable dv -> raise BytecodeError
  | _,Ast.Statement.DoWhile dw -> raise BytecodeError
  | _,Ast.Statement.Empty -> raise BytecodeError
  | _,Ast.Statement.ExportDefaultDeclaration edd -> raise BytecodeError
  | _,Ast.Statement.ExportNamedDeclaration end' -> raise BytecodeError
  | _,Ast.Statement.Expression expr ->
     generate_stm_expr venv expr
  | _,Ast.Statement.For for' -> raise BytecodeError
  | _,Ast.Statement.ForIn fi -> raise BytecodeError
  | _,Ast.Statement.ForOf fo -> raise BytecodeError
  | _,Ast.Statement.FunctionDeclaration fd ->
     generate_function_declaration venv fd
  | _,Ast.Statement.If if' -> generate_if venv if'
  | _,Ast.Statement.ImportDeclaration id -> raise BytecodeError
  | _,Ast.Statement.InterfaceDeclaration id -> raise BytecodeError
  | _,Ast.Statement.Labeled lab -> raise BytecodeError
  | _,Ast.Statement.Return ret ->
     generate_return venv ret
  | _,Ast.Statement.Switch sw -> raise BytecodeError
  | _,Ast.Statement.Throw thr -> raise BytecodeError
  | _,Ast.Statement.Try t -> raise BytecodeError
  | _,Ast.Statement.TypeAlias ta -> raise BytecodeError
  | _,Ast.Statement.OpaqueType ot -> raise BytecodeError
  | _,Ast.Statement.VariableDeclaration vd ->
     generate_declarations venv vd
  | _,Ast.Statement.While w -> generate_while venv w
  | _,Ast.Statement.With w -> raise BytecodeError

and generate_while venv = function
  | { Ast.Statement.While.test=cond;
      Ast.Statement.While.body=body } ->
     let cond' = generate_expression venv cond in
     let body' = generate_statement venv body in
     let push_body = Push (Code (body' @ [EndBody])) in
     let push_cond = Push (Code (cond' @ [JmpWhileTrue])) in
     [push_body] @ [push_cond] @ [WhileCond]
                                    
and generate_if venv = function
  | { Ast.Statement.If.test=cond;
      Ast.Statement.If.consequent=cons;
      Ast.Statement.If.alternate=alt } ->
     let cond' = generate_expression venv cond in
     let cons' = Push (Code ((generate_statement venv cons) @ [JmpTop])) in
     let alt' = Push (Code (begin match alt with
     | None -> []
     | Some alt -> generate_statement venv alt end @ [JmpTop])) in
     [cons'] @ [alt'] @ cond' @ [JmpTrue] 
    
and generate_return venv = function
  | { Ast.Statement.Return.argument=None } -> []
  | { Ast.Statement.Return.argument=Some arg } ->
     (generate_expression venv arg) @ [ActivationRevert]

and generate_function venv = function
  | { Ast.Function.id=_;
      Ast.Function.params=params;
      Ast.Function.body=body;
      Ast.Function.async=async;
      Ast.Function.generator=generator;
      Ast.Function.predicate=predicate;
      Ast.Function.return=return;
      Ast.Function.tparams=tparams; _ } ->
     let venv' = Env.create_env venv in
     let body' = generate_body venv' body in
     let body'' = match List.rev body' with
       | ActivationRevert::code -> body'
       | code -> body' @ [ActivationRevert] in
     let body''' = (generate_params_declaration venv' params) @ body'' in
     [(Push (Code body'''))]
     
and generate_function_declaration venv = function
  | { Ast.Function.id=Some (_,{ Ast.Identifier.name=id; _ });
      Ast.Function.params=params;
      Ast.Function.body=body;
      Ast.Function.async=async;
      Ast.Function.generator=generator;
      Ast.Function.predicate=predicate;
      Ast.Function.return=return;
      Ast.Function.tparams=tparams; _ } as f ->
     Env.insert venv id |> ignore;
     let code = generate_function venv f in
     [(Push (JSValue (String id)))] @
     code @
     [Bind]
  | { Ast.Function.id=None } ->
     Log.error "Function declaration expects a name";
     raise BytecodeError

and generate_param_declaration venv = function
  | _, { Ast.Function.Param.argument=arg; _ } ->
     match arg with
     | _,Ast.Pattern.Identifier(
             { Ast.Pattern.Identifier.name=_,{ Ast.Identifier.name=id; _ }; _}) ->
        Env.insert venv id |> ignore;
        (generate_pattern venv arg) @ [ActivationNameArg]
     | _ -> raise BytecodeError
       
and generate_params_declaration venv = function
  | _, { Ast.Function.Params.params=params; _ } ->
     let ids = List.map (generate_param_declaration venv) params in
     let ids' = List.flatten ids in
     match (List.length ids) > 0 with
     | false -> []
     | true  -> ids'

and generate_block' venv = function
  | { Ast.Statement.Block.body=body } ->
     generate_statements venv body
                         
and generate_block venv = function
  | _, block -> generate_block' venv block
                         
and generate_body venv = function
  | Ast.Function.BodyBlock block ->
     generate_block venv block
  | Ast.Function.BodyExpression expr ->
     generate_expression venv expr
                                           
and generate_declaration venv = function
  | _, { Ast.Statement.VariableDeclaration.Declarator.id=id;
         Ast.Statement.VariableDeclaration.Declarator.init=init } ->
     let id_gen = generate_pattern venv id in
     let id' = match id with
       | _,Ast.Pattern.Identifier
             { Ast.Pattern.Identifier.name=_,name; _} -> name
       | _ -> raise BytecodeError in
     Env.insert venv id'.name |> ignore;
     match init with
     | None -> [ActivationAllocLocal; Push (JSValue Undefined)] @
               id_gen @
               [ActivationPush]
     | Some expr ->
        let code = generate_expression venv expr in
        [ActivationAllocLocal] @
        code @
        id_gen @
        [ActivationPush]

and generate_pattern venv = function
  | _,Ast.Pattern.Identifier id ->
     generate_identifier' venv id
  | _ -> raise BytecodeError
                          
and generate_identifier' venv = function
  | { Ast.Pattern.Identifier.name=_,{ Ast.Identifier.name=id; _ }; _} ->
     [Push (JSValue (String id))]
       
and generate_declarations venv = function
  | { Ast.Statement.VariableDeclaration.declarations=decls;
      Ast.Statement.VariableDeclaration.kind=kind } ->
     List.flatten (List.map (generate_declaration venv)  decls)
                                    
and generate_stm_expr venv = function
  | { Ast.Statement.Expression.expression=expr;
      Ast.Statement.Expression.directive=dir } ->
     generate_expression venv expr

and generate_expression venv = function
  | _, Ast.Expression.Binary b ->
     generate_binary venv b
  | _, Ast.Expression.Array a ->
     Log.error "Found array";
     raise BytecodeError
  | _, Ast.Expression.ArrowFunction af -> raise BytecodeError
  | _, Ast.Expression.Assignment ass ->
     generate_assignment venv ass
  | _, Ast.Expression.Call call ->
     generate_call venv call
  | _, Ast.Expression.Class cl -> raise BytecodeError
  | _, Ast.Expression.Comprehension comp -> raise BytecodeError
  | _, Ast.Expression.Conditional cond -> raise BytecodeError
  | _, Ast.Expression.Function f -> generate_function venv f
  | _, Ast.Expression.Generator gen -> raise BytecodeError
  | _, Ast.Expression.Identifier i ->
     generate_identifier venv i
  | _, Ast.Expression.Import import -> raise BytecodeError
  | _, Ast.Expression.JSXElement jsx -> raise BytecodeError
  | _, Ast.Expression.JSXFragment jsx -> raise BytecodeError
  | _, Ast.Expression.Literal lit ->
     generate_literal venv lit                                        
  | _, Ast.Expression.Logical logical -> raise BytecodeError
  | _, Ast.Expression.Member mem ->
     generate_member venv mem
  | _, Ast.Expression.MetaProperty mp -> raise BytecodeError
  | _, Ast.Expression.New n ->
     generate_new venv n
  | _, Ast.Expression.Object obj -> raise BytecodeError
  | _, Ast.Expression.Sequence seq -> raise BytecodeError
  | _, Ast.Expression.Super -> raise BytecodeError
  | _, Ast.Expression.TaggedTemplate tt -> raise BytecodeError
  | _, Ast.Expression.TemplateLiteral tl -> raise BytecodeError
  | _, Ast.Expression.This -> raise BytecodeError
  | _, Ast.Expression.TypeCast tc -> raise BytecodeError
  | _, Ast.Expression.Unary un -> raise BytecodeError
  | _, Ast.Expression.Update up -> raise BytecodeError
  | _, Ast.Expression.Yield y -> raise BytecodeError
  | _, Ast.Expression.OptionalCall oc -> raise BytecodeError
  | _, Ast.Expression.OptionalMember om -> raise BytecodeError

and generate_new venv = function
  | { Ast.Expression.New.callee=callee;
      Ast.Expression.New.targs=targs;
      Ast.Expression.New.arguments=args } ->
     let callee' = generate_expression venv callee in
     let load_args =
       List.mapi (fun i arg ->
           let arg' = generate_expression_or_spread venv arg in
           arg' @ [ActivationPushArg]) args in
     callee' @
       [ActivationCreate] @
           (List.flatten load_args) @
             [ActivationSwitchConstructor]
    
and generate_member venv = function
  | { Ast.Expression.Member._object=obj;
      Ast.Expression.Member.property=prop } ->
     let base = match obj with
     | _, Ast.Expression.Identifier (_,{Ast.Identifier.name=id}) ->
        [Push (JSValue (String id))];
     | expr -> generate_expression venv expr in
     begin match prop with
     | Ast.Expression.Member.PropertyIdentifier id ->
        begin match id with
        | _, { Ast.Identifier.name=name } ->
           base @ [Push (JSValue (String name)); LoadMember]
          end;
     | Ast.Expression.Member.PropertyExpression expr ->
        raise BytecodeError
     | Ast.Expression.Member.PropertyPrivateName name ->
        raise BytecodeError end
     
and generate_call venv = function
  | { Ast.Expression.Call.callee=callee;
      Ast.Expression.Call.targs=targs;
      Ast.Expression.Call.arguments=args } ->
     let callee' = generate_expression venv callee in
     let load_args =
       List.mapi (fun i arg ->
           let arg' = generate_expression_or_spread venv arg in
           arg' @ [ActivationPushArg]) args in
     callee' @
       [ActivationCreate] @
           (List.flatten load_args) @
             [ActivationSwitch]

and generate_expression_or_spread venv = function
  | Ast.Expression.Spread spread -> raise BytecodeError
  | Ast.Expression.Expression expr -> generate_expression venv expr
      
and generate_assignment venv = function
  | { Ast.Expression.Assignment.operator=op;
      Ast.Expression.Assignment.left=pattern;
      Ast.Expression.Assignment.right=expr } ->
     let lhs = generate_pattern venv pattern in
     let rhs = generate_expression venv expr in
     rhs @ lhs @ [ActivationStore; ActivationLoad]
               
and generate_binary venv = function
  | { Ast.Expression.Binary.operator=op;
      Ast.Expression.Binary.left=left;
      Ast.Expression.Binary.right=right; } ->
     let code : t = generate_expression venv left @
                    generate_expression venv right @
                    generate_binary_op venv op in code

and generate_binary_op venv = function
  | Ast.Expression.Binary.Equal -> raise BytecodeError
  | Ast.Expression.Binary.NotEqual -> raise BytecodeError
  | Ast.Expression.Binary.StrictEqual -> raise BytecodeError      
  | Ast.Expression.Binary.StrictNotEqual -> raise BytecodeError
  | Ast.Expression.Binary.LessThan -> [LessThan]
  | Ast.Expression.Binary.LessThanEqual -> [LessThanEqualTo]
  | Ast.Expression.Binary.GreaterThan -> [GreaterThan]
  | Ast.Expression.Binary.GreaterThanEqual -> [GreaterThanEqualTo]
  | Ast.Expression.Binary.LShift -> raise BytecodeError
  | Ast.Expression.Binary.RShift -> raise BytecodeError
  | Ast.Expression.Binary.RShift3 -> raise BytecodeError
  | Ast.Expression.Binary.Plus -> [Plus]
  | Ast.Expression.Binary.Minus -> [Minus]
  | Ast.Expression.Binary.Mult -> [Multiply]
  | Ast.Expression.Binary.Exp -> raise BytecodeError
  | Ast.Expression.Binary.Div -> [Divide]
  | Ast.Expression.Binary.Mod -> raise BytecodeError
  | Ast.Expression.Binary.BitOr -> raise BytecodeError
  | Ast.Expression.Binary.Xor -> raise BytecodeError
  | Ast.Expression.Binary.BitAnd -> raise BytecodeError
  | Ast.Expression.Binary.In -> raise BytecodeError
  | Ast.Expression.Binary.Instanceof -> raise BytecodeError

and generate_identifier venv = function
  | loc,{ Ast.Identifier.name=name; _ } ->
     begin match Env.lookup venv name with
     | None        ->
        Log.warn "%s is not declared. This could result in a runtime error at %s" name (Loc.show loc);
     | Some offset -> () end;
     [Push(JSValue (String name))]
                                              
and generate_literal venv = function
  | { Ast.Literal.value=Ast.Literal.Number lit; _ } ->
     [(Push (JSValue (Number (Real lit))))]
  | { Ast.Literal.value=Ast.Literal.String lit; _ } ->
     raise BytecodeError
  | { Ast.Literal.value=Ast.Literal.Boolean lit; _ } ->
     [(Push (JSValue (Boolean lit)))]
  | { Ast.Literal.value=Ast.Literal.RegExp lit; _ } ->
     raise BytecodeError
  | _ ->
     raise BytecodeError

