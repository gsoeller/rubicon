var a = 1;

function foo() {
    var b = 2;
    function bar() {
        return a + b;
    }
    b = 3;
    return bar();
}

foo();
