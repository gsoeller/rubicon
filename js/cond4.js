function foo(a, b) {
    if (a > b) {
        if (a < 10) {
            return a;
        } else {
            return 1;
        }
    } else {
        if (b < 10) {
            return b
        } else {
            return 2;
        }
    }
    return 0;
}

foo(11,0); // 1
