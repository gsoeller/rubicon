function factorial(a) {
    var i = 1;
    var acc = 1;
    while (1) {
        if (i > a) {
            return acc;
        } else {
            acc = acc * i;
            i = i + 1;
        }
    }
}

factorial(5); // 120
