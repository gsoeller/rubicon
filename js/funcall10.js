function foo() {
    function bar() {
        return 5;
    }
    bar();
}

function bar() {
    return 1;
}

foo();
