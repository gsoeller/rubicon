function bar() {
    return 1;
}

function foo() {
    var a = bar();
    function bar() {
        return 5;
    }
    bar() + a;
}

foo() + bar();
