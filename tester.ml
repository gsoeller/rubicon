open OUnit2
open Vm
       
let tests = [
  ("Simple 1", "js/simple1.js", Number (Real 8.));
  ("Simple 2", "js/simple2.js", Number (Real (-4.)));
  
  ("Variable 0", "js/var0.js", Undefined);
  ("Variable 1", "js/var1.js", Undefined);
  ("Variable 2", "js/var2.js", Number (Real 1.));
  ("Variable 3", "js/var3.js", Number (Real 3.));
  ("Variable 4", "js/var4.js", Number (Real 3.));
  ("Variable 5", "js/var5.js", Number (Real 4.));
  ("Variable 6", "js/var6.js", Number (Real 1.));
  ("Variable 7", "js/var7.js", Number (Real 6.));

  ("Boolean 1", "js/bool1.js", Boolean true);
  ("Boolean 2", "js/bool2.js", Boolean false);
  ("Boolean 3", "js/bool3.js", Number (Real 2.));
  ("Boolean 4", "js/bool4.js", Number (Real 1.));
  ("Boolean 5", "js/bool5.js", Number (Real 1.));
  ("Boolean 6", "js/bool6.js", Number (Real 0.));
 
  
  ("Funcall 1", "js/funcall1.js", Number (Real 1.));
  ("Funcall 2", "js/funcall2.js", Number (Real 3.));
  ("Funcall 3", "js/funcall3.js", Number (Real 2.));
  ("Funcall 4", "js/funcall4.js", Number (Real 6.));
  ("Funcall 5", "js/funcall5.js", Number (Real 6.));
  ("Funcall 6", "js/funcall6.js", Number (Real 5.));
  ("Funcall 7", "js/funcall7.js", Number (Real 15.));
  ("Funcall 8", "js/funcall8.js", Number (Real 1.));
  ("Funcall 9", "js/funcall9.js", Undefined);
  ("Funcall 10", "js/funcall10.js", Number (Real 5.));
  ("Funcall 11", "js/funcall11.js", Number (Real 6.));
  ("Funcall 12", "js/funcall12.js", Number (Real 7.));
  ("Funcall 13", "js/funcall13.js", Number (Real 7.));
  ("Funcall 14", "js/funcall14.js", Number (Real 6.));
  
  ("Closure 1", "js/closure1.js", Number (Real 3.));
  ("Closure 2", "js/closure2.js", Number (Real 4.));

  ("Conditional 1", "js/cond1.js", Number (Real 1.));
  ("Conditional 2", "js/cond2.js", Number (Real 0.));
  ("Conditional 3", "js/cond3.js", Number (Real 5.));
  ("Conditional 4", "js/cond4.js", Number (Real 1.));
  ("Conditional 5", "js/cond5.js", Number (Real 6.));
  ("Conditional 6", "js/cond6.js", Number (Real 2.));
  
  ("While 1", "js/while1.js", Number (Real 0.));
  ("While 2", "js/while2.js", Number (Real 10.));
  ("While 3", "js/while3.js", Number (Real 211.));
  (*"While 4", "js/while4.js", Number 120.);*)

  ("NaN 1", "js/nan1.js", Number NaN);
  ("NaN 2", "js/nan2.js", Number NaN);

  ("Objects 1", "js/object1.js", Number (Real (2. ** -52.)));
  ("Objects 2", "js/object2.js", Boolean true);
  ("Objects 3", "js/object3.js", Boolean false);
  ("Objects 4", "js/object4.js", Number (Real 1.));
  ("Objects 5", "js/object5.js", Number (Real 0.));
  ("Objects 6", "js/object6.js", Number NaN);
]

    
let run vm (module Interp : Rubicon.Bytecode.Interpreter) js_file (expected : Vm.js_type) test_ctxt =
  (*let interpreter = (module Rubicon.Bytecode.Willy : Rubicon.Bytecode.Interpreter) in
  let module Willy = (val interpreter : Rubicon.Bytecode.Interpreter) in*)
  let ast,errors = Rubicon.Parse.parse js_file in
  let bc = Rubicon.Bytecode.generate_bytecode ast in       
  let actual = Interp.interpret vm bc in
  match (Vm.show_js_type actual) = (Vm.show_js_type expected) with
  | true -> assert_bool "" true
  | false ->
     let msg = "Expected `" ^ (Vm.show_js_type expected) ^
                 "`, but found `" ^ (Vm.show_js_type actual) ^ "`" in
     assert_bool msg false
                 
let run_sahara js_file (expected : Vm.js_type) test_ctx =
  let interpreter = (module Rubicon.Bytecode.Sahara : Rubicon.Bytecode.Interpreter) in
  let vm = Vm.initializeVM () in
  run vm interpreter js_file expected test_ctx;
  Vm.cleanupVM vm

let summary = "Rubicon test suite"
let readme = "Execute this code to run the rubicon test suite"
let debug_doc = "Turns on debug information"
              
let test_command =
  Core.Command.basic_spec
  ~summary:summary
  ~readme:(fun () -> readme)
  Core.Command.Spec.(
    empty +>
      flag "-debug" no_arg ~doc:debug_doc
  )
  (fun debug () ->
    Log.color_on ();
    begin match debug with
    | false -> ()
    | true  ->
       Log.set_log_level Log.DEBUG;
    end;
    let sahara_suite = List.map (fun (name, js_file, expected) ->
                           name>:: (run_sahara js_file expected))
                         tests in
    run_test_tt_main ("Sahara tests">:::sahara_suite)
  )

let () =
  Core.Command.run
    ~version:"0.1"
    ~build_info:"Rubicon test suite"
    test_command
