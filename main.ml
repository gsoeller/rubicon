
external initialize_heap : unit -> unit = "initializeHeap"
       
let () =
  let open Core.Command.Let_syntax in
  Core.Command.basic
    ~summary:"Rubicon JavaScript engine"
    [%map_open
       (*let num_eggs =
         flag "num-eggs" (optional int) ~doc:"COUNT cook this many eggs"*)
      let js_file =
        anon ("js_file" %: string)
      in
      fun () ->
        Log.set_log_level Log.DEBUG;
        Log.color_on ();
        Log.debug "Interpreting %s" js_file;
        let ast,errors = Rubicon.Parse.parse js_file in
        let anf,errors' = ast,errors(*Rubicon.Anf.transform (ast,errors) in*) in
        let s = Rubicon.Parse.show_program' anf in
        Log.info "%s" s;

        
        
        let vm = Vm.initializeVM () in

        (*
          generate_bytecode should generate a closure. A closure
          includes a name, an environment of bound variables, an
          environment of free variables, and code.

          The environment of bound variables should be updated
          with pointers into the heap. This way, free variables
          in a lower scope can directly point to the right 
          variable.
        *)
        let bc = Rubicon.Bytecode.generate_bytecode anf in
        (*Rubicon.Bytecode.emit_bytecode bc;*)
        Log.info "Bytecode...\n%s" (Rubicon.Bytecode.show bc);
        Log.info "Interpreting sahara...";
        let res = Rubicon.Bytecode.Sahara.interpret vm bc in
        Log.info "Sahara result: %s" (Vm.show_js_type res);
        Vm.cleanupVM vm;
        (*)let willy_value = Rubicon.Bytecode.Willy.interpret bc in
        Log.info "Result: %s" (Vm.show_js_type willy_value)*)
    ]
  |> Core.Command.run
                                     
